package com.sidechainlabs.infomanggung.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 11/25/2017.
 */

public class ListNotificationsAdapter extends RecyclerView.Adapter{

    private Context mContext;
    private List<mGigs> gigsList;

    public ListNotificationsAdapter(Context mContext, List<mGigs> gigsList){
        this.mContext = mContext;
        this.gigsList = gigsList;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item,parent,false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return gigsList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.gigs_name)
        TextView mItemName;
        @BindView(R.id.gigs_date)
        TextView mItemDate;
        @BindView(R.id.gigs_location)
        TextView mItemLocation;
//        @BindView(R.id.artist_name)
//        TextView mItemArtistName;


        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mGigs gigs = gigsList.get(position);
            mItemName.setText(gigs.getName());
//            mItemArtistName.setText(gigs.getArtistName());
            Date date = null;
            if(!gigs.getEventDate().equalsIgnoreCase("")){
                try {
                    date = new SimpleDateFormat(GlobalValue.FORMAT_DATE).parse(gigs.getEventDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat dayFormat = new SimpleDateFormat("EEEE");
                DateFormat numberDayFormat = new SimpleDateFormat("dd");
                DateFormat monthFormat = new SimpleDateFormat("MMMM");
                DateFormat yearFormat = new SimpleDateFormat("yyyy");
                String dayFromDate = dayFormat.format(date);
                String numberDayFromDate = numberDayFormat.format(date);
                String monthFromDate = monthFormat.format(date);
                String yearFromDate = yearFormat.format(date);
                mItemDate.setText(dayFromDate + " "+numberDayFromDate + " "+monthFromDate + " "+yearFromDate);
            }else{
                mItemDate.setText("");
            }
            mItemLocation.setText(gigs.getPlace());

        }

        @Override
        public void onClick(View view) {

        }
    }
}
