package com.sidechainlabs.infomanggung.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.CustomGlide.DisplayImage;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.model.mGigs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 11/1/2017.
 */

public class ListGigsAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<mGigs> gigsList;
    private String data;

    public ListGigsAdapter(Context mContext, List<mGigs> gigsList) {
        this.mContext = mContext;
        this.gigsList = gigsList;
    }
    public ListGigsAdapter(Context mContext, List<mGigs> gigsList, String data) {
        this.mContext = mContext;
        this.gigsList = gigsList;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(data!=null){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gigs_item_search,parent,false);
            return new ListViewHolderSearch(view);

        }else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gigs_item,parent,false);
            return new ListViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(data!=null){
            ((ListViewHolderSearch) holder).bindView(position);
        }else{
            ((ListViewHolder) holder).bindView(position);

        }
    }

    @Override
    public int getItemCount() {
        return gigsList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.itemName) TextView mItemText;
        @BindView(R.id.itemImage) ImageView mItemImage;
        @BindView(R.id.itemArtists) TextView mItemArtists;
        @BindView(R.id.itemEventDate) TextView mItemEventDate;
        @BindView(R.id.itemEventMonth) TextView mItemEventMonth;
        @BindView(R.id.itemPlace) TextView mItemPlace;



        public ListViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mGigs gigs = gigsList.get(position);
            mItemText.setText(gigs.getName());
            String artists = "";
            if(null!=gigs.getArtists() && gigs.getArtists().length()>150){
                artists = gigs.getArtists().substring(0,60)+"...";
            }else if(null!=gigs.getArtists()){
                artists = gigs.getArtists();
            }
            artists = artists.replaceAll(",$", "");
            mItemArtists.setText(artists);
            Date date = null;
            try {
                date = new SimpleDateFormat(GlobalValue.FORMAT_DATE).parse(gigs.getEventDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat numberDayFormat = new SimpleDateFormat("dd");
            DateFormat monthFormat = new SimpleDateFormat("MMMM");
            String numberDayFromDate = numberDayFormat.format(date);
            String monthFromDate = monthFormat.format(date);
            mItemEventDate.setText(numberDayFromDate);
            mItemEventMonth.setText(monthFromDate);
            mItemPlace.setText(gigs.getPlace()+", "+gigs.getLocation().getCityName());
            RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext).load(gigs.getPhotoCoverThumbnail());
            Glide.with(mContext).load(gigs.getPhotoCover()).thumbnail(thumbnailRequest).into(mItemImage);
            //use private s3 aws
//            RequestBuilder<Drawable> thumbnailRequest = DisplayImage.getInstance().getImaage(mContext,gigs.getPhotoCoverThumbnail());
//            DisplayImage.getInstance().displayImageForUser(mContext, mItemImage, gigs.getPhotoCover(), thumbnailRequest);


        }

        @Override
        public void onClick(View view) {

        }
    }

    public class ListViewHolderSearch extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.itemName) TextView mItemText;
        @BindView(R.id.itemPlace) TextView mItemPlace;
        @BindView(R.id.itemLocation) TextView mItemLocation;
        @BindView(R.id.itemEventDate) TextView mItemEventDate;
        @BindView(R.id.itemEventMonth) TextView mItemEventMonth;


        public ListViewHolderSearch(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mGigs gigs = gigsList.get(position);
            mItemText.setText(gigs.getName());
            Date date = null;
            try {
                date = new SimpleDateFormat(GlobalValue.FORMAT_DATE).parse(gigs.getEventDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            mItemPlace.setText(gigs.getPlace());
            mItemLocation.setText(gigs.getLocation().getCityName());
            DateFormat numberDayFormat = new SimpleDateFormat("dd");
            DateFormat monthFormat = new SimpleDateFormat("MMMM");
            String numberDayFromDate = numberDayFormat.format(date);
            String monthFromDate = monthFormat.format(date);
            mItemEventDate.setText(numberDayFromDate);
            mItemEventMonth.setText(monthFromDate);

            //use private s3 aws
//            RequestBuilder<Drawable> thumbnailRequest = DisplayImage.getInstance().getImaage(mContext,gigs.getPhotoCoverThumbnail());
//            DisplayImage.getInstance().displayImageForUser(mContext, mItemImage, gigs.getPhotoCover(), thumbnailRequest);


        }

        @Override
        public void onClick(View view) {

        }
    }
}

