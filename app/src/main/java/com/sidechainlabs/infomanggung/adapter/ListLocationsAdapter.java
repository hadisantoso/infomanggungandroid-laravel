package com.sidechainlabs.infomanggung.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.model.mGigs;
import com.sidechainlabs.infomanggung.model.mLocation;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by Santoso on 12/3/2017.
 */

public class ListLocationsAdapter extends RecyclerView.Adapter{

    private Context mContext;
    private List<mLocation> locationsList;
    private String deletelocation = "deletelocation";
    private final ListLocationsAdapter listLocationsAdapter = this;
    private String data;

    public ListLocationsAdapter(Context mContext, List<mLocation> locationsList){
        this.mContext = mContext;
        this.locationsList = locationsList;
    }

    public ListLocationsAdapter(Context mContext, List<mLocation> locationsList, String data){
        this.mContext = mContext;
        this.locationsList = locationsList;
        this.data = data;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_item,parent,false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return locationsList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.name)
        TextView mItemName;
        @BindView(R.id.delete_location)
        ImageView mDeleteItem;


        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mDeleteItem.setOnClickListener(this);
            if(data!=null){
                mDeleteItem.setVisibility(View.GONE);
            }
        }

        public void bindView(int position) {
            mLocation location = locationsList.get(position);
            mItemName.setText(location.getCityName());
        }

        @Override
        public void onClick(View view) {
            final mLocation location = locationsList.get(getAdapterPosition());
            JSONObject reqObj = new JSONObject();
            try {
                reqObj.put("location_id",location.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            GeneralMethods.requestData(deletelocation, reqObj, mContext, new Callback() {
                @Override
                public void getData(String data) {
                    locationsList.remove(getAdapterPosition());
                    listLocationsAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Throwable error) {
                    if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                            || error instanceof ConnectTimeoutException) {
                        Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
