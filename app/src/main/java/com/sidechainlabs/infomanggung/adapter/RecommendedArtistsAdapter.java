package com.sidechainlabs.infomanggung.adapter;

/**
 * Created by Santoso on 10/28/2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.model.mArtist;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class RecommendedArtistsAdapter extends RecyclerView.Adapter<RecommendedArtistsAdapter.MyViewHolder> {

    private Context mContext;
    private List<mArtist> artistList;
    private int artistPostiion;
    private String followartist = "followartist";
    private String unfollowartist = "unfollowartist";
    private final String FOLLOW = "Follow";
    private final String UNFOLLOW = "Unfollow";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);
        }
    }


    public RecommendedArtistsAdapter(Context mContext, List<mArtist> artistList) {
        this.mContext = mContext;
        this.artistList = artistList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommend_artist_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        mArtist artist = artistList.get(position);
        holder.title.setText(artist.getName());

        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext).load(artist.getPhotoProfileThumbnail());
        Glide.with(mContext).load(artist.getPhotoProfile()).thumbnail(thumbnailRequest).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                artistPostiion = position;
                showPopupMenu(holder.overflow);
            }
        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        //inflater.inflate(R.menu.menu_artist, popup.getMenu());
        mArtist artist = artistList.get(artistPostiion);
        if(artist.isFollowed()){
            popup.getMenu().add(UNFOLLOW);
        }else{
            popup.getMenu().add(FOLLOW);
        }
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            final mArtist artist = artistList.get(artistPostiion);
            JSONObject reqObj = new JSONObject();
            try {
                reqObj.put("artist_id",artist.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            switch (menuItem.getTitle().toString()) {
                case FOLLOW:
                    artist.setFollowed(true);
                    GeneralMethods.requestData(followartist, reqObj, mContext, new Callback() {
                        @Override
                        public void getData(String data) {
                            Toast.makeText(mContext, "Add "+artist.getName(), Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    return true;
                case UNFOLLOW:
                    artist.setFollowed(false);
                    GeneralMethods.requestData(unfollowartist, reqObj, mContext, new Callback() {
                        @Override
                        public void getData(String data) {
                            Toast.makeText(mContext, "Remove "+artist.getName(), Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(mContext,mContext.getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }
}