package com.sidechainlabs.infomanggung.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.model.mArtist;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 11/29/2017.
 */

public class ProfileListArtistsAdapter extends RecyclerView.Adapter{
    private Context mContext;
    private List<mArtist> artistList;


    public ProfileListArtistsAdapter(Context mContext, List<mArtist> artistList) {
        this.mContext = mContext;
        this.artistList = artistList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_artists_item,parent,false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.itemName)
        TextView mItemText;
        @BindView(R.id.itemImage)
        ImageView mItemImage;

        public ListViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mArtist artist = artistList.get(position);
            mItemText.setText(artist.getName());
            RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext).load(artist.getPhotoProfileThumbnail());
            Glide.with(mContext).load(artist.getPhotoProfile()).thumbnail(thumbnailRequest).into(mItemImage);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
