package com.sidechainlabs.infomanggung.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.model.mArtist;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 11/3/2017.
 */

public class GigsDetailsArtistAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<mArtist> artistsList;

    public GigsDetailsArtistAdapter(Context mContext, List<mArtist> artistsList) {
        this.mContext = mContext;
        this.artistsList = artistsList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gigs_details_artists_item,parent,false);
        return new GigsDetailsArtistAdapter.ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return artistsList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.artist_image)
        ImageView mItemImage;
        @BindView(R.id.artist_name)
        TextView mItemName;


        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mArtist artist = artistsList.get(position);
            if(null!=artist){
                mItemName.setText(artist.getName());
                Glide.with(mContext).load(artist.getPhotoProfileThumbnail()).into(mItemImage);
            }else{
                mItemName.setText(mContext.getResources().getText(R.string.no_artists_available));
                mItemName.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View view) {

        }
    }
}
