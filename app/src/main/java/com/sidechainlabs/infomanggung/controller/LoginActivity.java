package com.sidechainlabs.infomanggung.controller;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.base.PermissionsUtils;
import com.sidechainlabs.infomanggung.custommodel.LoginRequest;
import com.sidechainlabs.infomanggung.custommodel.LoginResponse;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

import com.appsee.Appsee;


import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class LoginActivity extends AppCompatActivity {
    public final String GOOGLE_PROVIDER = "GOOGLE";
    public final String FACEBOOK_PROVIDER = "FACEBOOK";
    public final String EMAIL_PROVIDER = "EMAIL";
    private static final int RC_SIGN_IN = 1;
    private final int RC_FACEBOOK = CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode();
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private static final String TAG = "LOGIN_ACTIVITY";
    private mUser muser;
    private ProgressDialog progressDialog;
    private boolean isGrantedMusicRead = true;

    private Gson gson = new Gson();

    private String registerUri = "login";

    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Appsee.start();
        /*Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                FirebaseCrash.report(ex);
            }
        });*/

        PermissionsUtils.requestReadExternalPermission(this);
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            //Start Get Permssion Marsmallow
//            ActivityCompat.requestPermissions(LoginActivity.this,
//                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                    1);
////            ActivityCompat.requestPermissions(LoginActivity.this,
////                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
////                    2);
//            //end Get permssion Marsmallow
//        }
//        else{
//            isGrantedMusicRead = true;
//        }


        muser = new mUser();
        setContentView(R.layout.activity_login);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        GradientDrawable  drawable = (GradientDrawable) this.findViewById(R.id.signInEmail).getBackground();
        drawable.setColor(getResources().getColor(R.color.orange_login));
        drawable = (GradientDrawable) this.findViewById(R.id.signInfFbBtn).getBackground();
        drawable.setColor(getResources().getColor(R.color.blue_fb));
        drawable = (GradientDrawable) this.findViewById(R.id.signInGoogleBtn).getBackground();
        drawable.setColor(getResources().getColor(R.color.red_google));

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(LoginActivity.this,"Connection Failed",Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mAuth = FirebaseAuth.getInstance();

        //Log.d(TAG,"create");

        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");
                        muser.setProvider(FACEBOOK_PROVIDER);
                        muser.setId(loginResult.getAccessToken().getUserId());
                        firebaseAuthWithGoogle(loginResult.getAccessToken().getToken(),FACEBOOK_PROVIDER);

                    }

                    @Override
                    public void onCancel() {
                        stopProgressDialog();
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        stopProgressDialog();
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void signInGoogle(View view) {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signInFacebook(View view) {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

    }

    public void signUpEmail(View view){
        Intent intent = new Intent(LoginActivity.this, SignupEmailActivity.class);
        startActivity(intent);
    }

    public void loginByEmail(View view){
        Intent intent = new Intent(LoginActivity.this, LoginByEmailActivity.class);
        startActivityForResult(intent,10);
    }

    public void forgotPassword(View view){
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void signInEmail(String email, String password){
//        String email = (String)((EditText)findViewById(R.id.emailEditText)).getText().toString();
//        String password = (String)((EditText)findViewById(R.id.passwordEditText)).getText().toString();
//        if(email.isEmpty()|| password.isEmpty()){
//            Toast.makeText(this,getResources().getText(R.string.empty_email_password),Toast.LENGTH_LONG).show();
//            return;
//        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sign In...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user.isEmailVerified()==false){
                                Toast.makeText(LoginActivity.this,"Please verify your email",Toast.LENGTH_LONG).show();
                                stopProgressDialog();
                                return;
                            }
                            muser.setId(user.getUid());
                            muser.setFullName(user.getDisplayName());
                            muser.setEmail(user.getEmail());
                            muser.setProvider(EMAIL_PROVIDER);
                            user.getIdToken(true)
                                    .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                                            if (task.isSuccessful()) {
                                                try{
                                                    String idToken = task.getResult().getToken();
                                                    muser.setFirebaseIdToken(idToken);
                                                    GeneralMethods.saveObjectData(LoginActivity.this,"user",muser);
                                                    LoginRequest loginRequest = new LoginRequest();
                                                    loginRequest.setId(muser.getId());
                                                    loginRequest.setFirebaseIdToken(muser.getFirebaseIdToken());
                                                    loginRequest.setProvider(muser.getProvider());
                                                    loginRequest.setFirebaseInstanceIdToken(FirebaseInstanceId.getInstance().getToken());

                                                    JsonParser parser = new JsonParser();
                                                    String reqString = gson.toJson(loginRequest);
                                                    JSONObject reqObj = new JSONObject(parser.parse(reqString).getAsJsonObject().toString());
                                                    GeneralMethods.requestData(registerUri, reqObj, LoginActivity.this, new Callback() {
                                                        @Override
                                                        public void getData(String data) {
                                                            Log.i("aaa",data);
                                                            LoginResponse loginResponse = gson.fromJson(data,LoginResponse.class);
                                                            stopProgressDialog();
//                                                            if(loginResponse.getIsNewUser().equalsIgnoreCase("1")) {
                                                                if (isGrantedMusicRead == true) {
                                                                    GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
                                                                    Intent intent = new Intent(LoginActivity.this, RecommendedArtistActivity.class);
                                                                    startActivity(intent);
                                                                    finish();
                                                                } else {
                                                                    GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
                                                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                                    GeneralMethods.saveData(LoginActivity.this,"isRecommended","1");
                                                                    startActivity(intent);
                                                                    finish();
                                                                }
//                                                            }
//                                                            else{
//                                                                GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
//                                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                                                GeneralMethods.saveData(LoginActivity.this,"isRecommended","1");
//                                                                startActivity(intent);
//                                                                finish();
//                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Throwable error) {
                                                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                                                    || error instanceof ConnectTimeoutException) {
                                                                mAuth.signOut();
                                                                stopProgressDialog();
                                                                Toast.makeText(LoginActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                                                            }
                                                            else{
                                                                mAuth.signOut();
                                                                stopProgressDialog();
                                                                Toast.makeText(LoginActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });

                                                }catch (Exception e){
                                                    Log.e(TAG,"Login Activity - Error");
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                // Handle error -> task.getException();
                                            }
                                        }
                                    });
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Wrong Email or Password",
                                    Toast.LENGTH_SHORT).show();
                            stopProgressDialog();
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sign In...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                final GoogleSignInAccount account = result.getSignInAccount();
                muser.setId(account.getId());
                muser.setFullName(account.getDisplayName());
                muser.setEmail(account.getEmail());
                muser.setGoogleIdToken(account.getIdToken());
                muser.setProvider(GOOGLE_PROVIDER);
                firebaseAuthWithGoogle(account.getIdToken(),GOOGLE_PROVIDER);
            } else {
                Toast.makeText(this,"Connection Failed",Toast.LENGTH_SHORT);
                stopProgressDialog();
            }
        }

        if (FacebookSdk.isFacebookRequestCode(requestCode)){
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == 10){
            if(resultCode == 10){
                signInEmail(data.getStringExtra("email"), data.getStringExtra("password"));
            }else{
                stopProgressDialog();
            }
        }

    }

    private void firebaseAuthWithGoogle(final String token,final String provider) {
        AuthCredential credential = null;
        if(provider.equalsIgnoreCase(GOOGLE_PROVIDER)){
            credential = GoogleAuthProvider.getCredential(token, null);
        }
        else if (provider.equalsIgnoreCase(FACEBOOK_PROVIDER)){
            credential = FacebookAuthProvider.getCredential(token);
        }
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        mAuth.getCurrentUser().getIdToken(true)
                                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                                        if (task.isSuccessful()) {
                                            try{
                                                String idToken = task.getResult().getToken();
                                                muser.setFirebaseIdToken(idToken);
                                                GeneralMethods.saveObjectData(LoginActivity.this,"user",muser);
                                                LoginRequest loginRequest = new LoginRequest();
                                                loginRequest.setId(muser.getId());
                                                loginRequest.setFirebaseIdToken(muser.getFirebaseIdToken());
                                                loginRequest.setProvider(muser.getProvider());
                                                loginRequest.setFirebaseInstanceIdToken(FirebaseInstanceId.getInstance().getToken()); //for push notification
                                                if(provider.equalsIgnoreCase(GOOGLE_PROVIDER)){
                                                    loginRequest.setGoogleIdToken(token);
                                                }
                                                else if (provider.equalsIgnoreCase(FACEBOOK_PROVIDER)){
                                                    loginRequest.setFacebookIdtoken(token);
                                                }

//                                                StringEntity entity = new StringEntity(gson.toJson(loginRequest));
                                                JsonParser parser = new JsonParser();
                                                String reqString = gson.toJson(loginRequest);
                                                JSONObject reqObj = new JSONObject(parser.parse(reqString).getAsJsonObject().toString());
                                                GeneralMethods.requestData(registerUri, reqObj, LoginActivity.this, new Callback() {
                                                    @Override
                                                    public void getData(String data) {
                                                        Log.i("aaa",data);
                                                        LoginResponse loginResponse = gson.fromJson(data,LoginResponse.class);
                                                        stopProgressDialog();
//                                                        if(null!=loginResponse.getIsNewUser()) {
                                                            if (isGrantedMusicRead == true) {
                                                                GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
                                                                Intent intent = new Intent(LoginActivity.this, RecommendedArtistActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                            } else {
                                                                GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
                                                                GeneralMethods.saveData(LoginActivity.this,"isRecommended","1");
                                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                            }
//                                                            }
//                                                            else{
//                                                                GeneralMethods.saveObjectData(LoginActivity.this, "user", loginResponse.getUser());
//                                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                                                GeneralMethods.saveData(LoginActivity.this,"isRecommended","1");
//                                                                startActivity(intent);
//                                                                finish();
//                                                            }

                                                    }

                                                    @Override
                                                    public void onFailure(Throwable error) {
                                                        if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                                                || error instanceof ConnectTimeoutException) {
                                                            mAuth.signOut();
                                                            stopProgressDialog();
                                                            Toast.makeText(LoginActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                                                        }
                                                        else{
                                                            mAuth.signOut();
                                                            stopProgressDialog();
                                                            Toast.makeText(LoginActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }catch (Exception e){
                                                Log.e(TAG,"Login Activity - Error");
                                                e.printStackTrace();
                                            }
                                        } else {
                                            // Handle error -> task.getException();
                                        }
                                    }
                                });
                    }

                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 4: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGrantedMusicRead = true;
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(LoginActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    private void stopProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG,"start");
        String isRecommended = GeneralMethods.getData(this,"isRecommended","");
        if (mAuth.getCurrentUser()!=null){
            if(isRecommended.equalsIgnoreCase("")){
                Intent intent = new Intent(LoginActivity.this, RecommendedArtistActivity.class);
                startActivity(intent);
                finish();
            }
            else{
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,"stop");

    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(TAG,"pause");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG,"Destroy");
    }
}
