package com.sidechainlabs.infomanggung.controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;


import com.appsee.Appsee;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.fragments.BaseFragment;
import com.sidechainlabs.infomanggung.fragments.DetailsGigsFragment;
import com.sidechainlabs.infomanggung.fragments.GigsTabsFragment;
import com.sidechainlabs.infomanggung.fragments.LocationsFragment;
import com.sidechainlabs.infomanggung.fragments.ProfileFragment;
import com.sidechainlabs.infomanggung.fragments.SearchFragment;
import com.sidechainlabs.infomanggung.fragments.SettingsFragment;
import com.sidechainlabs.infomanggung.fragments.ArtistsFragment;
import com.sidechainlabs.infomanggung.fragments.NotificationsFragment;
import com.sidechainlabs.infomanggung.model.mLocation;
import com.sidechainlabs.infomanggung.utils.FragmentHistory;
import com.sidechainlabs.infomanggung.utils.Utils;
import com.sidechainlabs.infomanggung.views.FragNavController;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class MainActivity extends BaseActivity implements BaseFragment.FragmentNavigation, FragNavController.TransactionListener, FragNavController.RootFragmentListener {

    String addlocationuri = "addlocation";
    private Gson gson = new Gson();

    @BindView(R.id.content_frame)
    FrameLayout contentFrame;

    @BindView(R.id.toolbar)
    Toolbar toolbar;



    private int[] mTabIconsSelected = {
            R.drawable.ic_home_icon,
            R.drawable.ic_artis_icon,
            R.drawable.ic_search_icon,
            R.drawable.ic_profile_icon,
            R.drawable.ic_settings_black_24dp};


    @BindArray(R.array.tab_name)
    String[] TABS;

    @BindView(R.id.bottom_tab_layout)
    TabLayout bottomTabLayout;

    private FragNavController mNavController;

    private FragmentHistory fragmentHistory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Appsee.start();
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);


        initToolbar();

        initTab();

        fragmentHistory = new FragmentHistory();


        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content_frame)
                .transactionListener(this)
                .rootFragmentListener(this, TABS.length)
                .build();


        switchTab(0);

        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                fragmentHistory.push(tab.getPosition());

                switchTab(tab.getPosition());


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Fragment f = mNavController.getCurrentFrag();
                if (f != null){
                    if(f instanceof GigsTabsFragment){
                        Fragment subFragment =((GigsTabsFragment) f).getTabFragment();
                        View fragmentView = subFragment.getView();
                        AppBarLayout appBarLayout = f.getView().findViewById(R.id.appbar);
                        if(appBarLayout != null){
                            appBarLayout.setExpanded(true);
                        }

                        RecyclerView mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycle_view);
                        if (mRecyclerView != null)
                            mRecyclerView.scrollToPosition(0);
                    }else{
                        View fragmentView = f.getView();
                        RecyclerView mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycle_view);
                        if (mRecyclerView != null)
                            mRecyclerView.scrollToPosition(0);
                    }
                }


                mNavController.clearStack();

                switchTab(tab.getPosition());


            }
        });


        //Intent for NOTIFICATION
        Intent i = getIntent();
        Bundle extras = i.getExtras();

        if(extras != null) {
            String push = extras.getString("push");
            if (push != null) {
                mNavController.switchTab(FragNavController.TAB1);
                updateTabSelection(FragNavController.TAB1);
                DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                pushFragment(detailsGigsFragment.newInstance(extras.getString("gigs_id"),extras.getString("gigs_name")));
            }else if (  extras.getString("gigs_id") != null  ){
                mNavController.switchTab(FragNavController.TAB1);
                updateTabSelection(FragNavController.TAB1);
                DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                pushFragment(detailsGigsFragment.newInstance(extras.getString("gigs_id"),extras.getString("gigs_name")));
            }else {
                mNavController.switchTab(FragNavController.TAB1);
                updateTabSelection(FragNavController.TAB1);
            }
        }else {
            mNavController.switchTab(FragNavController.TAB1);
            updateTabSelection(FragNavController.TAB1);
        }

        if(!isNetworkConnected()){
            GeneralMethods.cancelRequest();
            Toast.makeText(this,getResources().getText(R.string.connection_errormsg),Toast.LENGTH_LONG).show();
        }

    }

    private void initToolbar() {
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private void initTab() {
        if (bottomTabLayout != null) {
            for (int i = 0; i < TABS.length; i++) {
                bottomTabLayout.addTab(bottomTabLayout.newTab());
                TabLayout.Tab tab = bottomTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(getTabView(i));
            }
        }
    }


    private View getTabView(int position) {
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_item_bottom, null);
        ImageView icon = (ImageView) view.findViewById(R.id.tab_icon);
        icon.setImageDrawable(Utils.setDrawableSelector(MainActivity.this, mTabIconsSelected[position], mTabIconsSelected[position]));
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void switchTab(int position) {
        mNavController.switchTab(position);

//        updateToolbarTitle(position);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        GeneralMethods.cancelRequest();
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            if (fragmentHistory.isEmpty()) {
                super.onBackPressed();
            } else {
                if (fragmentHistory.getStackSize() > 1) {
                    int position = fragmentHistory.popPrevious();
                    switchTab(position);
                    TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(position);
                    selectedTab.select();
                    updateTabSelection(position);

                } else {
                    switchTab(0);
                    TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(0);
                    selectedTab.select();
                    updateTabSelection(0);
                    fragmentHistory.emptyStack();
                }
            }

        }
    }


    private void updateTabSelection(int currentTab){
        for (int i = 0; i <  TABS.length; i++) {
            TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(i);
            if(currentTab != i) {
                selectedTab.getCustomView().setSelected(false);
            }else{
                selectedTab.getCustomView().setSelected(true);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }


    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    private void updateToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setDisplayShowHomeEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }


    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case FragNavController.TAB1:
//                return new GigsFragment();
                return new GigsTabsFragment();
            case FragNavController.TAB2:
                return new ArtistsFragment();
            case FragNavController.TAB3:
                return new SearchFragment();
            case FragNavController.TAB4:
                return new ProfileFragment();
            case FragNavController.TAB5:
                return new SettingsFragment();
        }
        throw new IllegalStateException("Need to send an index that we know");
    }


//    private void updateToolbarTitle(int position){
//
//
//        getSupportActionBar().setTitle(TABS[position]);
//
//    }


    public void updateToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GlobalValue.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                addLocation(place.getId(),place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    private void addLocation(String googlePlaceId,CharSequence name){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("google_place_id",googlePlaceId);
            reqObj.put("city_name",name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(addlocationuri, reqObj, this, new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultLocation = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultLocation = jsonResult.getJSONObject("location");
                    data = jsonResultLocation.toString();
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mLocation gigsResponse = gson.fromJson(data,mLocation.class);
                LocationsFragment lf = (LocationsFragment) mNavController.getCurrentFrag();
                lf.addData(gigsResponse);
            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(MainActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(MainActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
