package com.sidechainlabs.infomanggung.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.GeneralMethods;

public class ForgotPasswordActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private static final String TAG = "FORGOT PASS ACTIVITY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }

    public void forgotPassword(View view){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String email = (String)((EditText)findViewById(R.id.emailEditText)).getText().toString();
        if(email.equalsIgnoreCase("")){
            Toast.makeText(ForgotPasswordActivity.this, "Please fill an email address...",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!GeneralMethods.isEmailValid(email)){
            Toast.makeText(ForgotPasswordActivity.this, "Please fill a valid email address",Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending email");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            stopProgressDialog();
                            Toast.makeText(ForgotPasswordActivity.this, "Please check your email to reset password",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            // If sign in fails, display a message to the user.
                            stopProgressDialog();
                            Log.w(TAG, "resetPassword:failure", task.getException());
                            Toast.makeText(ForgotPasswordActivity.this, task.getException().getMessage(),Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void stopProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }
}
