package com.sidechainlabs.infomanggung.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.custommodel.LoginRequest;
import com.sidechainlabs.infomanggung.custommodel.LoginResponse;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginByEmailActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "SIGNUP ACTIVITY";
    private ProgressDialog progressDialog;
    private mUser muser;
    public final String EMAIL_PROVIDER = "EMAIL";
    private AsyncHttpClient client = new AsyncHttpClient();
    private Gson gson = new Gson();
    private String registerUri = "login";
    private boolean isGrantedMusicRead = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_by_email);
        mAuth = FirebaseAuth.getInstance();
        muser = new mUser();
        GradientDrawable drawable = (GradientDrawable) this.findViewById(R.id.signInEmail).getBackground();
        drawable.setColor(getResources().getColor(R.color.orange_login));

    }

    public void signInEmail(View view){
        String email = (String)((EditText)findViewById(R.id.emailEditText)).getText().toString();
        String password = (String)((EditText)findViewById(R.id.passwordEditText)).getText().toString();
        if(email.isEmpty()|| password.isEmpty()){
            Toast.makeText(this,getResources().getText(R.string.empty_email_password),Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("email",email);
        intent.putExtra("password",password);
        setResult(10,intent);
        finish();
    }

    public void forgotPassword(View view){
        Intent intent = new Intent(LoginByEmailActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private void stopProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }
}
