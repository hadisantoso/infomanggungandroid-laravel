package com.sidechainlabs.infomanggung.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.RecommendedArtistsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.custommodel.RecommendArtistsResponse;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

public class RecommendedArtistActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecommendedArtistsAdapter artistAdapter;
    private List<mArtist> artistList;
    private AsyncHttpClient client = new AsyncHttpClient();
    private String recommendartists = "recommendartists";
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_artists);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        mUser user = (mUser) GeneralMethods.getObjectData(RecommendedArtistActivity.this, "user", null, mUser.class);
        getSupportActionBar().setTitle(R.string.recommends);
        recyclerView =  findViewById(R.id.recycler_view);

        artistList = new ArrayList<>();
        artistAdapter = new RecommendedArtistsAdapter(this, artistList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(artistAdapter);

        //READ MUSIC INFO
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] projection = {"DISTINCT " +
                MediaStore.Audio.Media.ARTIST,
        };

        JSONArray dataArtists = new JSONArray();
        Cursor cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);
        if (cursor.moveToFirst()) {
            do {
                String data = cursor.getString(cursor.getColumnIndex("artist"));
                //Log.d("artist", data);
                dataArtists.put(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("artists",dataArtists);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GeneralMethods.requestData(recommendartists, reqObj, this, new Callback() {
            @Override
            public void getData(String data) {
                RecommendArtistsResponse followArtistResponse = gson.fromJson(data,RecommendArtistsResponse.class);
                for(int i=0;i<followArtistResponse.getArtists().size();i++){
                    mArtist artist = followArtistResponse.getArtists().get(i);
                    artist.setPhotoProfile(followArtistResponse.getAwsPrefix()+artist.getPhotoProfile());
                    artist.setPhotoProfileThumbnail(followArtistResponse.getAwsPrefix()+artist.getPhotoProfile());
                    artistList.add(followArtistResponse.getArtists().get(i));
                }
                artistAdapter.notifyDataSetChanged();
                }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(RecommendedArtistActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(RecommendedArtistActivity.this,getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void goMain(View view){
        GeneralMethods.saveData(this,"isRecommended","1");
        Intent intent = new Intent(RecommendedArtistActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
