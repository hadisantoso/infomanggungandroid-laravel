package com.sidechainlabs.infomanggung.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.custommodel.LoginRequest;
import com.sidechainlabs.infomanggung.custommodel.LoginResponse;
import com.sidechainlabs.infomanggung.model.mUser;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.entity.StringEntity;

public class SignupEmailActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "SIGNUP ACTIVITY";
    private ProgressDialog progressDialog;
    private mUser muser;
    public final String EMAIL_PROVIDER = "EMAIL";
    private AsyncHttpClient client = new AsyncHttpClient();
    private Gson gson = new Gson();
    private String registerUri = "login";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_email);
        mAuth = FirebaseAuth.getInstance();
        muser = new mUser();

    }

    public void signUp(View view) {
        final String name = (String)((EditText)findViewById(R.id.nameEditText)).getText().toString();
        String email = (String)((EditText)findViewById(R.id.emailEditText)).getText().toString();
        String password = (String)((EditText)findViewById(R.id.passwordEditText)).getText().toString();
        String confirmPassword = (String)((EditText)findViewById(R.id.passwordConfirmEditText)).getText().toString();
        if(name.isEmpty()||email.isEmpty()||password.isEmpty()||confirmPassword.isEmpty()){
            Toast.makeText(this,getResources().getText(R.string.fill_empty_value),Toast.LENGTH_LONG).show();
            return;
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sign Up...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        if(password.equals(confirmPassword)){
            Log.i("Confirmed","true");
            progressDialog.show();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            if (task.isSuccessful()) {
//                                Log.d(TAG, "createUserWithEmail:success");
//                                FirebaseUser user = mAuth.getCurrentUser();
//                                user.sendEmailVerification();
//                                stopProgressDialog();
//                                Toast.makeText(SignupEmailActivity.this,getResources().getText(R.string.verify_email),Toast.LENGTH_SHORT).show();
//                                GeneralMethods.saveData(SignupEmailActivity.this,"full_name",name);
//                                Intent intent = new Intent(SignupEmailActivity.this, LoginActivity.class);
//                                mAuth.signOut();
//                                startActivity(intent);
//                                finish();

//                            } else {
//                                stopProgressDialog();
//                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
//                                Toast.makeText(SignupEmailActivity.this, "Authentication failed, "+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
//
//                            }
                            if (task.isSuccessful()) {
                                final FirebaseUser user = mAuth.getCurrentUser();
                                muser.setId(user.getUid());
                                muser.setFullName(user.getDisplayName());
                                muser.setEmail(user.getEmail());
                                muser.setProvider(EMAIL_PROVIDER);
                                user.getIdToken(true)
                                        .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                                if (task.isSuccessful()) {
                                                    try {
                                                        String idToken = task.getResult().getToken();
                                                        muser.setFirebaseIdToken(idToken);
                                                        GeneralMethods.saveObjectData(SignupEmailActivity.this, "user", muser);
                                                        LoginRequest loginRequest = new LoginRequest();
                                                        loginRequest.setId(muser.getId());
                                                        loginRequest.setFirebaseIdToken(muser.getFirebaseIdToken());
                                                        loginRequest.setProvider(muser.getProvider());
                                                        loginRequest.setFirebaseInstanceIdToken(FirebaseInstanceId.getInstance().getToken());
                                                        loginRequest.setName(name);
                                                        StringEntity entity = new StringEntity(gson.toJson(loginRequest));
                                                        client.setTimeout(GlobalValue.SOCKET_TIMEOUT);
                                                        client.post(SignupEmailActivity.this, GeneralMethods.getServiceUri(registerUri), entity, "application/json", new AsyncHttpResponseHandler() {
                                                            @Override
                                                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                                user.sendEmailVerification();
                                                                if (statusCode == HttpStatus.SC_OK) {
                                                                    Toast.makeText(SignupEmailActivity.this, getResources().getText(R.string.verify_email), Toast.LENGTH_SHORT).show();
                                                                    Intent intent = new Intent(SignupEmailActivity.this, LoginActivity.class);
                                                                    mAuth.signOut();
                                                                    startActivity(intent);
                                                                    finish();
                                                                }
                                                                stopProgressDialog();
                                                                mAuth.signOut();
                                                            }

                                                            @Override
                                                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                                                                        || error instanceof ConnectTimeoutException) {
                                                                    mAuth.signOut();
                                                                    stopProgressDialog();
                                                                    Toast.makeText(SignupEmailActivity.this, getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                                    mAuth.signOut();
                                                                    stopProgressDialog();
                                                                    Toast.makeText(SignupEmailActivity.this, getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        Log.e(TAG, "Login Activity - Error");
                                                        e.printStackTrace();
                                                    }
                                                } else {

                                                }
                                            }
                                        });
                            } else {
                                stopProgressDialog();
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(SignupEmailActivity.this, "Authentication failed, "+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        else{
            Toast.makeText(SignupEmailActivity.this,getResources().getText(R.string.password_not_match),Toast.LENGTH_SHORT).show();
        }
    }
    private void stopProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
        }
    }
}
