package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.model.mArtist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 12/5/2017.
 */

public class GigsTabsFragment extends BaseFragment{

    ViewPager viewPager;
    Adapter adapter;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(null!=view){
//            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
            return view;
        }

        view = inflater.inflate(R.layout.fragment_gigs_tabs,container, false);
        ButterKnife.bind(this,view);
        // Setting ViewPager for each Tabs
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);

        return view;

    }


    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new GigsFragment(), "Latest");
        adapter.addFragment(new GigsByRecommendationFragment(), "Recommended");
        adapter.addFragment(new GigsByPopularFragment(), "Popular");

        viewPager.setAdapter(adapter);



    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public Fragment getTabFragment(){

        Fragment page = adapter.getItem(viewPager.getCurrentItem());
        return page;
    }

    @Override
    public void onDestroyView() {
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        super.onDestroyView();
    }

    public void showNotif(View view){

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_show_notification, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_event:
                showNotif();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showNotif(){
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        if (mFragmentNavigation != null) {
            mFragmentNavigation.pushFragment(notificationsFragment.newInstance(0));

        }
    }

}
