package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListGigsAdapter;
import com.sidechainlabs.infomanggung.adapter.ListLocationsAdapter;
import com.sidechainlabs.infomanggung.adapter.ListNotificationsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.LocationResponse;
import com.sidechainlabs.infomanggung.custommodel.NotificationResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.model.mGigs;
import com.sidechainlabs.infomanggung.model.mLocation;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by Santoso on 12/1/2017.
 */

public class LocationsFragment extends BaseFragment{
    final String TAG= "LocationFragment";
    int fragCount;
    private List<mLocation> locationList;
    ListLocationsAdapter listLocationAdapter;
    String locationuri = "locations";
    private Gson gson = new Gson();
    private EndlessRecyclerViewScrollListener scrollListener;
    RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    ProgressBar progressBar;
    View view;

    @BindView(R.id.getlocations) FloatingActionButton locationsBtn;

    public static LocationsFragment newInstance(String instance) {
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, instance);
        LocationsFragment fragment = new LocationsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public LocationsFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //reattach view if fragment already created;
        ( (MainActivity)getActivity()).updateToolbarTitle("Locations");

        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_locations, container, false);
        ButterKnife.bind(this,view);
        locationsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                            .build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setFilter(typeFilter).build(getActivity());
                    getActivity().startActivityForResult(intent, GlobalValue.PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.i(TAG,e.getMessage());
                } catch (GooglePlayServicesNotAvailableException e) {
                    Log.i(TAG,e.getMessage());
                    Toast.makeText(getActivity(),getResources().getText(R.string.no_google_play_service),Toast.LENGTH_SHORT).show();
                }
            }
        });

        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        locationList = new ArrayList<>();
        listLocationAdapter = new ListLocationsAdapter(getActivity(),locationList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listLocationAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        progressBar.setVisibility(View.VISIBLE);
        swipeContainer.setVisibility(View.GONE);
        addData(1);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addData(1);
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);

        return view;
    }

    private void addData(final int page) {
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page", page);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(locationuri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                if (page == 1) {
                    locationList.clear();
                    scrollListener.resetState();
                    listLocationAdapter.notifyDataSetChanged();
                }
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("locations");
                    data = jsonResultGigs.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                LocationResponse locationResponse = gson.fromJson(data, LocationResponse.class);
                for (int i = 0; i < locationResponse.getLocations().size(); i++) {
                    mLocation locaion = locationResponse.getLocations().get(i);
                    locationList.add(locaion);
                }
                listLocationAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void addData(mLocation location){
        locationList.add(location);
        listLocationAdapter.notifyDataSetChanged();
    }

}
