package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.GigsDetailsArtistAdapter;
import com.sidechainlabs.infomanggung.adapter.ListGigsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.GigsArtistsResponse;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class DetailsGigsFragment extends BaseFragment {

    private String queryParam,queryName;
    private Gson gson = new Gson();
    private String getDetailsUri = "getgigsdetails";
    RecyclerView recyclerView;
    private List<mArtist> listArtist;
    private GigsDetailsArtistAdapter gigsDetailsArtistAdapter;
    mGigs gigs;
    private GoogleMap googleMap;
    private GigsArtistsResponse artists;
    private String awsPrefix = null;
    View view;
    boolean followed = false;
    private final String FOLLOW = "Follow";
    private final String UNFOLLOW ="Unfollow";
    private String followagigs = "followgigs";
    private String unfollowgigs = "unfollowgigs";

    @BindView(R.id.itemImage)
    ImageView itemImage;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.itemDescription)
    TextView description;
    @BindView(R.id.itemEventDate)
    TextView itemEventDate;
    @BindView(R.id.itemEventTime)
    TextView itemEventTime;
    @BindView(R.id.itemPlace)
    TextView itemPlace;
    @BindView(R.id.itemLocation)
    TextView itemLocation;
    @BindView(R.id.mapLinearLayout)
    LinearLayout mapLinearLayout;
    @BindView(R.id.getdirections)
    LinearLayout getDirections;
    @BindView(R.id.mapView)
    MapView mMapView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.details_scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.andmore)
    TextView andMore;
    @BindView(R.id.following)
    FloatingActionButton followingBtn;


    public DetailsGigsFragment() {
    }


    public static DetailsGigsFragment newInstance(String query, String name) {
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, query);
        args.putString("queryName", name);
        DetailsGigsFragment fragment = new DetailsGigsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            queryParam = getArguments().getString(ARGS_INSTANCE);
            queryName = getArguments().getString("queryName");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ( (MainActivity)getActivity()).updateToolbarTitle(queryName);
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_details_gigs, container, false);
        recyclerView = view.findViewById(R.id.gigs_details_artist_recycle_view);
        listArtist = new ArrayList<>();
        gigsDetailsArtistAdapter = new GigsDetailsArtistAdapter(getActivity(),listArtist);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(gigsDetailsArtistAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        ButterKnife.bind(this,view);
        mProgressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        getDetails(queryParam);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        getDirections.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent;
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="+gigs.getLatitude()+","+gigs.getLongitude()));
                startActivity(intent);

            }
        });
        andMore.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                for(int i=GlobalValue.AND_MORE_SIZE ; i<artists.getArtists().size();i++){
                    mArtist artist = artists.getArtists().get(i);
                    artist.setPhotoProfile(awsPrefix+artist.getPhotoProfile());
                    artist.setPhotoProfileThumbnail(awsPrefix+artist.getPhotoProfileThumbnail());
                    listArtist.add(artist);
                }
                gigsDetailsArtistAdapter.notifyDataSetChanged();
                andMore.setVisibility(View.GONE);
            }
        });
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mArtist artist = listArtist.get(position);
                        DetailsArtistFragment detailsArtistFragment = new DetailsArtistFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsArtistFragment.newInstance(artist.getId(),artist.getName()));

                        }
                    }
                })
        );
        followingBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                followingBtn.setClickable(false);
                JSONObject reqObj = new JSONObject();
                try {
                    reqObj.put("gigs_id",gigs.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(followed){
                    GeneralMethods.requestData(unfollowgigs, reqObj, getContext(), new Callback() {
                        @Override
                        public void getData(String data) {
                            followed=false;
                            followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add_black_24dp));
                            followingBtn.setClickable(true);
                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            followingBtn.setClickable(true);
                        }
                    });


                }else{
                    GeneralMethods.requestData(followagigs, reqObj, getContext(), new Callback() {
                        @Override
                        public void getData(String data) {
                            followed=true;
                            followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_delete_black_24dp));
                            followingBtn.setClickable(true);
                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            followingBtn.setClickable(true);
                        }
                    });

                }
            }
        });
        return view;
    }

    private void getDetails(String queryParam){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("id", queryParam);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(getDetailsUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;

                String gigsData = "";
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("gigs");
                    gigsData = jsonResultGigs.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                    followed = jsonResult.getBoolean("followed");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*Insert To Details Gigs*/
                gigs = gson.fromJson(gigsData,mGigs.class);
                if(followed){
                    followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_delete_black_24dp));

                }else{
                    followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add_black_24dp));
                }
                gigs.setPhotoCover(awsPrefix+gigs.getPhotoCover());
                gigs.setPhotoCoverThumbnail(awsPrefix+gigs.getPhotoCoverThumbnail());
                RequestBuilder<Drawable> thumbnailRequest = Glide.with(getActivity()).load(gigs.getPhotoCoverThumbnail());
                Glide.with(getActivity()).load(gigs.getPhotoCover()).thumbnail(thumbnailRequest).into(itemImage);
                name.setText(gigs.getName());
                description.setText(gigs.getDescription());
                Date date = null;
                try {
                    date = new SimpleDateFormat(GlobalValue.FORMAT_DATE).parse(gigs.getEventDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat dayFormat = new SimpleDateFormat("EEEE");
                DateFormat numberDayFormat = new SimpleDateFormat("dd");
                DateFormat monthFormat = new SimpleDateFormat("MMMM");
                DateFormat yearFormat = new SimpleDateFormat("yyyy");
                String dayFromDate = dayFormat.format(date);
                String numberDayFromDate = numberDayFormat.format(date);
                String monthFromDate = monthFormat.format(date);
                String yearFromDate = yearFormat.format(date);
                itemEventDate.setText(dayFromDate + " "+numberDayFromDate + " "+monthFromDate + " "+yearFromDate);
                String eventTime = new SimpleDateFormat("HH:mm").format(date);
                itemEventTime.setText(eventTime);
                itemPlace.setText(gigs.getPlace());
                itemLocation.setText(gigs.getLocation().getCityName());

                /*Insert To Artists Gigs*/
                artists = gson.fromJson(data,GigsArtistsResponse.class);
                if(artists.getArtists().size()>=GlobalValue.AND_MORE_SIZE){
                    for(int i=0 ; i<GlobalValue.AND_MORE_SIZE;i++){
                        mArtist artist = artists.getArtists().get(i);
                        artist.setPhotoProfile(awsPrefix+artist.getPhotoProfile());
                        artist.setPhotoProfileThumbnail(awsPrefix+artist.getPhotoProfileThumbnail());
                        listArtist.add(artist);
                    }
                    gigsDetailsArtistAdapter.notifyDataSetChanged();
                    if(artists.getArtists().size()!=GlobalValue.AND_MORE_SIZE){
                        andMore.setVisibility(View.VISIBLE);
                    }
                }
                else if(artists.getArtists().size()<GlobalValue.AND_MORE_SIZE){
                    for(int i=0 ; i<artists.getArtists().size();i++){
                        mArtist artist = artists.getArtists().get(i);
                        artist.setPhotoProfile(awsPrefix+artist.getPhotoProfile());
                        artist.setPhotoProfileThumbnail(awsPrefix+artist.getPhotoProfileThumbnail());
                        listArtist.add(artist);
                    }
                    gigsDetailsArtistAdapter.notifyDataSetChanged();
                }
                else{
                    listArtist.add(null);
                    gigsDetailsArtistAdapter.notifyDataSetChanged();
                }

                setDefaultMapLocation();

                mProgressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDefaultMapLocation(){
        if(null!=gigs.getLatitude() && null!=gigs.getLongitude()){
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap mMap) {
                    googleMap = mMap;

                    // For showing a move to my location button
                    //googleMap.setMyLocationEnabled(true);

                    LatLng location = new LatLng(Double.valueOf(gigs.getLatitude()), Double.valueOf(gigs.getLongitude()));
                    googleMap.addMarker(new MarkerOptions().position(location));

                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(15).build();
                    googleMap.getUiSettings().setAllGesturesEnabled(false);
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            });
        }else{
            mapLinearLayout.setVisibility(View.GONE);
            getDirections.setVisibility(View.GONE);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_add_event, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_event:
                addEvent();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addEvent(){
        Calendar cal = Calendar.getInstance();
        String myDate = gigs.getEventDate();
        SimpleDateFormat sdf = new SimpleDateFormat(GlobalValue.FORMAT_DATE);
        Date date= new Date();
        try{
        date = sdf.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, millis);
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, millis);
        intent.putExtra(CalendarContract.Reminders.MINUTES, 60);
        intent.putExtra("title", gigs.getName());
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
