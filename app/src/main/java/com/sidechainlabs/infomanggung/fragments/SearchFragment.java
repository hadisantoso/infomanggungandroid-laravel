package com.sidechainlabs.infomanggung.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListArtistsAdapter;
import com.sidechainlabs.infomanggung.adapter.ListGigsAdapter;
import com.sidechainlabs.infomanggung.adapter.ListLocationsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.ArtistsResponse;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.custommodel.LocationResponse;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;
import com.sidechainlabs.infomanggung.model.mLocation;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by Santoso on 3/21/2018.
 */

public class SearchFragment extends  BaseFragment{
    private String queryParam;
    private List<mGigs> gigsList;
    ListGigsAdapter listGigsAdapter;
    private List<mArtist> artistList;
    ListArtistsAdapter listArtistsAdapter;
    ListLocationsAdapter listLocationsAdapter;
    private List<mLocation> locationList;
    String searchGigsUri = "searchgigs";
    String searchArtistUri = "searchartists";
    String searchLocationUri = "searchlocation";
    Handler mHandler = new Handler();


    private Gson gson = new Gson();
    TextView gigsTitle;
    TextView artistTitle;
    TextView locationTitle;

    RecyclerView recyclerViewGigs;
    RecyclerView recyclerViewArtists;
    RecyclerView recyclerViewLocation;

    ProgressBar progressBar;
    int fragCount;
    SearchView searchView;
    View view;

    public SearchFragment(){

    }

    public static SearchFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, query);
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).updateToolbarTitle((fragCount == 0) ? "Search" : "Sub Search " + fragCount);
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_search, container, false);

        searchGigs();
        searchArtists();
        searchLocations();
        searchView = (SearchView) view.findViewById(R.id.search_view);
        searchView.setQueryHint("Let's Gigs");
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }
            @Override
            public boolean onQueryTextChange(final String query) {
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gigsList.clear();
                        listGigsAdapter.notifyDataSetChanged();
                        queryParam = query;
                        artistList.clear();
                        listArtistsAdapter.notifyDataSetChanged();
                        locationList.clear();
                        listLocationsAdapter.notifyDataSetChanged();
                        if(!query.equalsIgnoreCase("")){
                            progressBar.setVisibility(View.VISIBLE);
                            addDataGigs(1,queryParam);
                            addDataArtists(1,queryParam);
                            addDataLocations(1,queryParam);
                        }else{
                            gigsTitle.setVisibility(View.GONE);
                            artistTitle.setVisibility(View.GONE);
                            locationTitle.setVisibility(View.GONE);
                        }
                    }
                }, 300);

                return false;
            }
        });
        return  view;
    }
    private void searchGigs(){
        recyclerViewGigs = view.findViewById(R.id.recycle_view_gigs);
        gigsTitle = (TextView) view.findViewById(R.id.title_gigs);
        progressBar = view.findViewById(R.id.progress_bar);
        gigsList = new ArrayList<>();
        listGigsAdapter = new ListGigsAdapter(getActivity(),gigsList,"1");
        recyclerViewGigs.setHasFixedSize(true);
        recyclerViewGigs.setAdapter(listGigsAdapter);
        recyclerViewGigs.setNestedScrollingEnabled(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewGigs.setLayoutManager(layoutManager);
        recyclerViewGigs.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mGigs gigs = gigsList.get(position);
                        DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(), gigs.getName()));

                        }
                    }
                })
        );
    }

    private void searchArtists(){
        recyclerViewArtists = view.findViewById(R.id.recycle_view_artist);
        artistTitle = (TextView) view.findViewById(R.id.title_artist);
        progressBar = view.findViewById(R.id.progress_bar);
        artistList = new ArrayList<>();
        listArtistsAdapter = new ListArtistsAdapter(getActivity(),artistList);
        recyclerViewArtists.setHasFixedSize(true);
        recyclerViewArtists.setAdapter(listArtistsAdapter);
        recyclerViewArtists.setNestedScrollingEnabled(false);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);        recyclerViewArtists.setLayoutManager(layoutManager);
        recyclerViewArtists.setLayoutManager(layoutManager);
        recyclerViewArtists.setItemAnimator(new DefaultItemAnimator());
        recyclerViewArtists.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mArtist artist = artistList.get(position);
                        DetailsArtistFragment detailsArtistFragment = new DetailsArtistFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsArtistFragment.newInstance(artist.getId(), artist.getName()));

                        }
                    }
                })
        );
    }

    private void searchLocations(){
        recyclerViewLocation = view.findViewById(R.id.recycle_view_location);
        locationTitle = (TextView) view.findViewById(R.id.title_location);
        progressBar = view.findViewById(R.id.progress_bar);
        locationList = new ArrayList<>();
        listLocationsAdapter = new ListLocationsAdapter(getActivity(),locationList,"1");
        recyclerViewLocation.setHasFixedSize(true);
        recyclerViewLocation.setAdapter(listLocationsAdapter);
        recyclerViewLocation.setNestedScrollingEnabled(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewLocation.setLayoutManager(layoutManager);
        recyclerViewLocation.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mLocation location = locationList.get(position);
                        GigsByLocationsFragment gigsByLocationsFragment = new GigsByLocationsFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(gigsByLocationsFragment.newInstance(location.getId(), location.getCityName()));

                        }
                    }
                })
        );
    }

    private void addDataGigs(int page, String queryParam) {
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page", page);
            reqObj.put("query_param", queryParam);
            reqObj.put("is_search_all", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(searchGigsUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("gigs");
                    data = jsonResultGigs.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GigsResponse gigsResponse = gson.fromJson(data, GigsResponse.class);
                for (int i = 0; i < gigsResponse.getGigs().size(); i++) {
                    mGigs gig = gigsResponse.getGigs().get(i);
                    gig.setPhotoCover(awsPrefix + gig.getPhotoCover());
                    gig.setPhotoCoverThumbnail(awsPrefix + gig.getPhotoCoverThumbnail());
                    gigsList.add(gig);
                }
                if(gigsResponse.getGigs().size()==0){
                    gigsTitle.setVisibility(View.GONE);
                }else{
                    gigsTitle.setVisibility(View.VISIBLE);
                }
                listGigsAdapter.notifyDataSetChanged();
                recyclerViewGigs.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addDataArtists(int page, String queryParam){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page",page);
            reqObj.put("query_param", queryParam);
            reqObj.put("is_search_all", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(searchArtistUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultArtists = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultArtists = jsonResult.getJSONObject("artists");
                    data = jsonResultArtists.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArtistsResponse artistsResponse = gson.fromJson(data,ArtistsResponse.class);
                for(int i = 0; i< artistsResponse.getArtists().size(); i++){
                    mArtist artist = artistsResponse.getArtists().get(i);
                    artist.setPhotoProfile(awsPrefix+artist.getPhotoProfile());
                    artist.setPhotoProfileThumbnail(awsPrefix+artist.getPhotoProfileThumbnail());
                    artistList.add(artist);
                }
                if(artistsResponse.getArtists().size()==0){
                    artistTitle.setVisibility(View.GONE);
                }else{
                    artistTitle.setVisibility(View.VISIBLE);
                }
                listArtistsAdapter.notifyDataSetChanged();
                recyclerViewArtists.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void addDataLocations(int page, String queryParam){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page", page);
            reqObj.put("query_param", queryParam);
            reqObj.put("is_search_all", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(searchLocationUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("locations");
                    data = jsonResultGigs.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                LocationResponse locationResponse = gson.fromJson(data, LocationResponse.class);
                if(locationResponse.getLocations().size()==0){
                    locationTitle.setVisibility(View.GONE);
                }else{
                    locationTitle.setVisibility(View.VISIBLE);
                }
                for (int i = 0; i < locationResponse.getLocations().size(); i++) {
                    mLocation location = locationResponse.getLocations().get(i);
                    locationList.add(location);
                }
                listLocationsAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
