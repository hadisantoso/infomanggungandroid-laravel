package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListGigsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mGigs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class SearchGigsFragment extends BaseFragment {

    private String queryParam;
    private List<mGigs> gigsList;
    ListGigsAdapter listGigsAdapter;
    String searchGigsUri = "searchgigs";
    private Gson gson = new Gson();
    private SwipeRefreshLayout swipeContainer;
    private EndlessRecyclerViewScrollListener scrollListener;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    View view;

    public SearchGigsFragment() {
        // Required empty public constructor
    }


    public static SearchGigsFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, query);
        SearchGigsFragment fragment = new SearchGigsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            queryParam = getArguments().getString(ARGS_INSTANCE);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ( (MainActivity)getActivity()).updateToolbarTitle("");
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_gigs, container, false);
        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        gigsList = new ArrayList<>();
        listGigsAdapter = new ListGigsAdapter(getActivity(),gigsList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listGigsAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        addData(1,queryParam);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page,queryParam);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mGigs gigs = gigsList.get(position);
                        DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(), gigs.getName()));

                        }
                    }
                })
        );
        swipeContainer.setEnabled(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) searchItem.getActionView();
        searchView.setQuery(queryParam,true);
        searchView.setIconified(false);
        searchView.clearFocus();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // Do something
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                gigsList.clear();
                scrollListener.resetState();
                listGigsAdapter.notifyDataSetChanged();
                queryParam = query;
                addData(1,queryParam);
                clearSearchViewFocus();
                return true;
            }
        };

        searchView.setOnQueryTextListener(queryTextListener);
    }

    private void addData(int page, String queryParam) {
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page", page);
            reqObj.put("query_param", queryParam);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(searchGigsUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("gigs");
                    data = jsonResultGigs.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GigsResponse gigsResponse = gson.fromJson(data, GigsResponse.class);
                for (int i = 0; i < gigsResponse.getGigs().size(); i++) {
                    mGigs gig = gigsResponse.getGigs().get(i);
                    gig.setPhotoCover(awsPrefix + gig.getPhotoCover());
                    gig.setPhotoCoverThumbnail(awsPrefix + gig.getPhotoCoverThumbnail());
                    gigsList.add(gig);
                }
                listGigsAdapter.notifyDataSetChanged();
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
