package com.sidechainlabs.infomanggung.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.LoginActivity;
import com.sidechainlabs.infomanggung.controller.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SettingsFragment extends BaseFragment{
    private FirebaseAuth mAuth;

    @BindView(R.id.logoutBtn)
    Button logoutBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        ButterKnife.bind(this, view);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout(v);
            }
        });

        ButterKnife.bind(this, view);

        ( (MainActivity)getActivity()).updateToolbarTitle("Profile");

        mAuth = FirebaseAuth.getInstance();
        return view;
    }

    public void logout(View view){
        Log.i("logout","test");
        android.webkit.CookieManager.getInstance().removeAllCookie();
        mAuth.signOut();
        GeneralMethods.clearData(getActivity());
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}
