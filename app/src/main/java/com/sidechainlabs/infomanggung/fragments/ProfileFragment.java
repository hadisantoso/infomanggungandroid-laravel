package com.sidechainlabs.infomanggung.fragments;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListNotificationsAdapter;
import com.sidechainlabs.infomanggung.adapter.ProfileListArtistsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.LoginActivity;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.ArtistsResponse;
import com.sidechainlabs.infomanggung.custommodel.NotificationResponse;
import com.sidechainlabs.infomanggung.custommodel.ProfileResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class ProfileFragment extends BaseFragment{

    @BindView(R.id.photo_profile) ImageView photoProfile;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.locations) Button locationsBtn;
    @BindView(R.id.count_following) TextView countFollowing;
    @BindView(R.id.count_gigs) TextView countGigs;
    @BindView(R.id.count_locations) TextView countLocations;
    @BindView(R.id.content_frame )
    RelativeLayout contentLayout;
    @BindView(R.id.no_artist_layout)
    LinearLayout noArtistLayout;

    private FirebaseAuth mAuth;
    View view;
    private List<mArtist> artistList;
    private List<mGigs> gigsList;

    private RecyclerView recyclerView;
    private RecyclerView recyclerViewGigs;

    ProfileListArtistsAdapter profileListArtistsAdapter;
    ListNotificationsAdapter listNotificationsAdapter;

    ProgressBar progressBar;
    private EndlessRecyclerViewScrollListener scrollListener;
    private Gson gson = new Gson();
    String artisturi ="followedartists";
    String countUri = "getcountdata";
    String gigsuri = "notifications";

    private SwipeRefreshLayout swipeContainer;

    public static ProfileFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ( (MainActivity)getActivity()).updateToolbarTitle("Profile");
        if(view != null){
            return view;
        }
        mAuth = FirebaseAuth.getInstance();

        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        mUser user = (mUser)GeneralMethods.getObjectData(getContext(),"user",null,mUser.class);
        Glide.with(getContext()).load(user.getPictureUri()).into(photoProfile);
        name.setText(user.getFullName());
        recyclerView = view.findViewById(R.id.recycle_view_artist);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        GradientDrawable drawable = (GradientDrawable) view.findViewById(R.id.locations).getBackground();
        drawable.setColor(getResources().getColor(R.color.colorPrimary));
        artistList = new ArrayList<>();
        profileListArtistsAdapter = new ProfileListArtistsAdapter(getActivity(),artistList);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(profileListArtistsAdapter);
        getCountData();
        addData(1);
        swipeContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mArtist artist = artistList.get(position);
                        DetailsArtistFragment detailsArtistFragment = new DetailsArtistFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsArtistFragment.newInstance(artist.getId(),artist.getName()));

                        }
                    }
                })
        );
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCountData();
                addData(1);
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);

        locationsBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LocationsFragment locationsFragment = new LocationsFragment();
                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(locationsFragment.newInstance(null));

                }
            }
        });

//        //add gigs data
//        recyclerViewGigs = view.findViewById(R.id.recycle_view_gigs);
//        gigsList = new ArrayList<>();
//        listNotificationsAdapter = new ListNotificationsAdapter(getActivity(),gigsList);
//        recyclerViewGigs.setHasFixedSize(true);
//        recyclerViewGigs.setAdapter(listNotificationsAdapter);
//        final LinearLayoutManager layoutManagerGigs = new LinearLayoutManager(getActivity());
//        recyclerViewGigs.setNestedScrollingEnabled(false);
//        recyclerViewGigs.setLayoutManager(layoutManagerGigs);
//        addDataGigs(1);
//        scrollListener = new EndlessRecyclerViewScrollListener(layoutManagerGigs) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                addDataGigs(++page);
//            }
//        };
//
//        recyclerViewGigs.addOnScrollListener(scrollListener);
//        recyclerViewGigs.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        mGigs gigs = gigsList.get(position);
//                        DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
//                        if (mFragmentNavigation != null) {
//                            mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(),gigs.getName()));
//
//                        }
//                    }
//                })
//        );

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        btnClickMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mFragmentNavigation != null) {
//                    mFragmentNavigation.pushFragment(ProfileFragment.newInstance(fragCount + 1));
//
//
//                }
//            }
//        });

    }

    private void addData(final int page){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page",page);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(artisturi, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {

                JSONObject jsonResult = null;
                JSONObject jsonResultArtists = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultArtists = jsonResult.getJSONObject("artists");
                    data = jsonResultArtists.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ArtistsResponse artistsResponse = gson.fromJson(data,ArtistsResponse.class);
                if(page == 1){
                    artistList.clear();
                    scrollListener.resetState();
                    profileListArtistsAdapter.notifyDataSetChanged();
                    if(artistsResponse.getArtists().size() == 0){
                        contentLayout.setVisibility(View.GONE);
                        noArtistLayout.setVisibility(View.VISIBLE);
                    }else{
                        contentLayout.setVisibility(View.VISIBLE);
                        noArtistLayout.setVisibility(View.GONE);
                    }
                }
                for(int i = 0; i< artistsResponse.getArtists().size(); i++){
                    mArtist gig = artistsResponse.getArtists().get(i);
                    gig.setPhotoProfile(awsPrefix+gig.getPhotoProfile());
                    gig.setPhotoProfileThumbnail(awsPrefix+gig.getPhotoProfileThumbnail());
                    artistList.add(gig);
                }
                profileListArtistsAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
            }
        });

    }

    private void getCountData(){
        JSONObject reqObj = new JSONObject();
        GeneralMethods.requestData(countUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                ProfileResponse profileResponse = gson.fromJson(data,ProfileResponse.class);
                countFollowing.setText(profileResponse.getCountArtists());
                countGigs.setText(profileResponse.getCountGigs());
                countLocations.setText(profileResponse.getCountLocations());
            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_profile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void logout(){
        android.webkit.CookieManager.getInstance().removeAllCookie();
        mAuth.signOut();
        GeneralMethods.clearData(getActivity());
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
