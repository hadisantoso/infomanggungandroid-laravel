package com.sidechainlabs.infomanggung.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by f22labs on 07/03/17.
 */

public class BaseFragment extends Fragment {

    public static final String ARGS_INSTANCE = "arg1";
    FirebaseAnalytics firebaseAnalytics;
    float scale;
    int padding_50dp;
    boolean load = false;
    FragmentNavigation mFragmentNavigation;
    SearchView searchView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scale = getResources().getDisplayMetrics().density;
        padding_50dp = (int) (50*scale + 0.5f);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        firebaseAnalytics.setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigation) context;
        }
    }

    public interface FragmentNavigation {
         void pushFragment(Fragment fragment);
    }

    public void clearSearchViewFocus(){
        searchView.clearFocus();
    }





}
