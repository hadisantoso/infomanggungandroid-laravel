package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListArtistsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.ArtistsResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class ArtistsFragment extends BaseFragment{


    int fragCount;
    private List<mArtist> artistList;
    private RecyclerView recyclerView;
    ListArtistsAdapter listArtistsAdapter;
    ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;
    private EndlessRecyclerViewScrollListener scrollListener;
    private Gson gson = new Gson();
    String artisturi ="artists";

    View view;


    public static ArtistsFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ArtistsFragment fragment = new ArtistsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public ArtistsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).updateToolbarTitle((fragCount == 0) ? "Artists" : "Sub Artists " + fragCount);
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_artists, container, false);
        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        artistList = new ArrayList<>();
        listArtistsAdapter = new ListArtistsAdapter(getActivity(),artistList);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listArtistsAdapter);
        addData(1);
        swipeContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page);
            }

            @Override
            public void loadProgressBar() {
                if(load){
                    recyclerView.setPadding(0,0,0,padding_50dp);
                    progressBar.setVisibility(View.VISIBLE);
                }else{
                    recyclerView.setPadding(0,0,0,0);
                    progressBar.setVisibility(View.GONE);
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mArtist artist = artistList.get(position);
                        DetailsArtistFragment detailsArtistFragment = new DetailsArtistFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsArtistFragment.newInstance(artist.getId(),artist.getName()));

                        }
                    }
                })
        );
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addData(1);
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void addData(final int page){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page",page);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        load = true;
        GeneralMethods.requestData(artisturi, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                if(page == 1){
                    artistList.clear();
                    scrollListener.resetState();
                    listArtistsAdapter.notifyDataSetChanged();
                }
                JSONObject jsonResult = null;
                JSONObject jsonResultArtists = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultArtists = jsonResult.getJSONObject("artists");
                    data = jsonResultArtists.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArtistsResponse artistsResponse = gson.fromJson(data,ArtistsResponse.class);
                for(int i = 0; i< artistsResponse.getArtists().size(); i++){
                    mArtist gig = artistsResponse.getArtists().get(i);
                    gig.setPhotoProfile(awsPrefix+gig.getPhotoProfile());
                    gig.setPhotoProfileThumbnail(awsPrefix+gig.getPhotoProfileThumbnail());
                    artistList.add(gig);
                }
                listArtistsAdapter.notifyDataSetChanged();
//                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
                load = false;
            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                load = false;
//                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) searchItem.getActionView();
        int searchImgId = android.support.v7.appcompat.R.id.search_button; // I used the explicit layout ID of searchview's ImageView
        EditText txtSearch = ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        txtSearch.setTextColor(Color.WHITE);
        ImageView v = (ImageView) searchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.ic_menu_search);

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // Do something
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("test","intenthandle");
                searchView.clearFocus();
                SearchArtistsFragment searchArtistsFragment = new SearchArtistsFragment();
                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(searchArtistsFragment.newInstance(query));

                }
                return true;
            }
        };

        searchView.setOnQueryTextListener(queryTextListener);
    }

}
