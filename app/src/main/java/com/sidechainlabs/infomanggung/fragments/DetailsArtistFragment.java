package com.sidechainlabs.infomanggung.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ArtistDetailsGigsAdapter;
import com.sidechainlabs.infomanggung.adapter.GigsDetailsArtistAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.ArtistGigsResponse;
import com.sidechainlabs.infomanggung.custommodel.GigsArtistsResponse;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class DetailsArtistFragment extends BaseFragment {

    private String queryParam,queryName;
    private String getDetailsUri = "getartistdetails";
    mArtist artist;
    boolean followed = false;
    private Gson gson = new Gson();
    private final String FOLLOW = "Follow";
    private final String UNFOLLOW ="Unfollow";
    private String followartist = "followartist";
    private String unfollowartist = "unfollowartist";
    RecyclerView recyclerView;
    private List<mGigs> listGigs;
    private ArtistDetailsGigsAdapter artistDetailsGigsAdapter;
    View view;

    @BindView(R.id.itemImage)
    ImageView itemImage;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.itemDescription)
    TextView description;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.details_scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.following)
    FloatingActionButton followingBtn;
    @BindView(R.id.twitter)
    ImageView twitter;
    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.instagram)
    ImageView instagram;
    @BindView(R.id.spotify)
    ImageView spotify;

    public DetailsArtistFragment() {
    }

    public static DetailsArtistFragment newInstance(String param1, String name) {
        DetailsArtistFragment fragment = new DetailsArtistFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, param1);
        args.putString("queryName", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            queryParam = getArguments().getString(ARGS_INSTANCE);
            queryName = getArguments().getString("queryName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ( (MainActivity)getActivity()).updateToolbarTitle(queryName);
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_details_artist, container, false);
        ButterKnife.bind(this,view);
        recyclerView = view.findViewById(R.id.artist_details_gigs_recycle_view);
        listGigs = new ArrayList<>();
        artistDetailsGigsAdapter = new ArtistDetailsGigsAdapter(getActivity(),listGigs);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(artistDetailsGigsAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mProgressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        getDetails(queryParam);
        followingBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                followingBtn.setClickable(false);
                JSONObject reqObj = new JSONObject();
                try {
                    reqObj.put("artist_id",artist.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(followed){
                    GeneralMethods.requestData(unfollowartist, reqObj, getContext(), new Callback() {
                        @Override
                        public void getData(String data) {
                            followed=false;
                            followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add_black_24dp));
                            followingBtn.setClickable(true);
                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            followingBtn.setClickable(true);
                        }
                    });


                }else{
                    GeneralMethods.requestData(followartist, reqObj, getContext(), new Callback() {
                        @Override
                        public void getData(String data) {
                            followed=true;
                            followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_delete_black_24dp));
                            followingBtn.setClickable(true);
                        }

                        @Override
                        public void onFailure(Throwable error) {
                            if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                                    || error instanceof ConnectTimeoutException) {
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                            }
                            followingBtn.setClickable(true);
                        }
                    });

                }
            }
        });
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mGigs gigs = listGigs.get(position);
                        if(gigs == null){
                            return;
                        }
                        if(!gigs.getEventDate().equalsIgnoreCase("")){
                            DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                            if (mFragmentNavigation != null) {
                                mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(),gigs.getName()));

                            }
                        }
                    }
                })
        );
        twitter.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getTwitter()));
                startActivity(intent);
            }
        });
        facebook.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getFacebook()));
                startActivity(intent);
            }
        });
        instagram.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getInstagram()));
                startActivity(intent);
            }
        });
        spotify.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SpotifyWebViewFragment spotifyWebViewFragment = new SpotifyWebViewFragment();
                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(spotifyWebViewFragment.newInstance(artist.getSpotify(),artist.getName()));

                }
            }
        });

        return view;
    }

    private void getDetails(String queryParam){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("artist_id", queryParam);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(getDetailsUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultArtist = null;
                String awsPrefix = null;
                String artistData = "";

                try {
                    jsonResult = new JSONObject(data);
                    jsonResultArtist = jsonResult.getJSONObject("artist");
                    artistData = jsonResultArtist.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                    followed = jsonResult.getBoolean("followed");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*Insert To Details Gigs*/
                artist = gson.fromJson(artistData,mArtist.class);
                if(followed){
                    followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_delete_black_24dp));

                }else{
                    followingBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add_black_24dp));
                }
                artist.setPhotoCover(awsPrefix+artist.getPhotoCover());
                artist.setPhotoCoverThumbnail(awsPrefix+artist.getPhotoCoverThumbnail());
                RequestBuilder<Drawable> thumbnailRequest = Glide.with(getActivity()).load(artist.getPhotoCoverThumbnail());
                Glide.with(getActivity()).load(artist.getPhotoCover()).thumbnail(thumbnailRequest).into(itemImage);
                name.setText(artist.getName());
                description.setText(artist.getBioIndo());

                ArtistGigsResponse gigs = gson.fromJson(data,ArtistGigsResponse.class);
                if(gigs.getGigs().size()!=0){
                    for(int i=0 ; i<gigs.getGigs().size();i++){
                        mGigs gig = gigs.getGigs().get(i);
                        listGigs.add(gig);
                    }
                    artistDetailsGigsAdapter.notifyDataSetChanged();
                }else{
                    listGigs.add(null);
                    artistDetailsGigsAdapter.notifyDataSetChanged();
                }
                mProgressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
            }
        });
    }


}
