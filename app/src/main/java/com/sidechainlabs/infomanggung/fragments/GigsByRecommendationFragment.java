package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListGigsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mGigs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by Santoso on 12/20/2017.
 */

public class GigsByRecommendationFragment extends BaseFragment {
    int fragCount;
    private List<mGigs> gigsList;
    ListGigsAdapter listGigsAdapter;
    String gigsuri = "gigsbyuserrecommendation";
    private Gson gson = new Gson();
    private EndlessRecyclerViewScrollListener scrollListener;
    RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    ProgressBar progressBar;
    View view;
    private SliderLayout mSlider;
    private RelativeLayout contentLayout;
    private LinearLayout noGigsLayout;



    public static GigsFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        GigsFragment fragment = new GigsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public GigsByRecommendationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //reattach view if fragment already created;
        ((MainActivity) getActivity()).updateToolbarTitle((fragCount == 0) ? "Gigs" : "Sub Gigs " + fragCount);
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_gigs, container, false);
        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        contentLayout = view.findViewById(R.id.content_frame);
        noGigsLayout = view.findViewById(R.id.no_gigs_layout);
        gigsList = new ArrayList<>();
        listGigsAdapter = new ListGigsAdapter(getActivity(), gigsList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listGigsAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        addData(1);
        swipeContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mGigs gigs = gigsList.get(position);
                        DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(), gigs.getName()));

                        }
                    }
                })
        );
        //ButterKnife.bind(this, view);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addData(1);
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }

        //hide slider
        mSlider = view.findViewById(R.id.slider);
        mSlider.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        btnClickMe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (mFragmentNavigation != null) {
//                    mFragmentNavigation.pushFragment(GigsFragment.newInstance(fragCount + 1));
//
//                }
//            }
//        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void addData(final int page) {
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page", page);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(gigsuri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {

                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("gigs");
                    data = jsonResultGigs.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GigsResponse gigsResponse = gson.fromJson(data, GigsResponse.class);

                if (page == 1) {
                    gigsList.clear();
                    scrollListener.resetState();
                    listGigsAdapter.notifyDataSetChanged();
                    if(gigsResponse.getGigs().size() == 0){
                        contentLayout.setVisibility(View.GONE);
                        noGigsLayout.setVisibility(View.VISIBLE);
                    }else{
                        contentLayout.setVisibility(View.VISIBLE);
                        noGigsLayout.setVisibility(View.GONE);
                    }
                }


                for (int i = 0; i < gigsResponse.getGigs().size(); i++) {
                    mGigs gig = gigsResponse.getGigs().get(i);
                    gig.setPhotoCover(awsPrefix + gig.getPhotoCover());
                    gig.setPhotoCoverThumbnail(awsPrefix + gig.getPhotoCoverThumbnail());
                    gigsList.add(gig);
                }
                listGigsAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Throwable error) {
                if (error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getText(R.string.errormsg), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) searchItem.getActionView();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // Do something
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                SearchGigsFragment searchGigsFragment = new SearchGigsFragment();
                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(searchGigsFragment.newInstance(query));

                }
                return true;
            }
        };

        searchView.setOnQueryTextListener(queryTextListener);
    }
}
