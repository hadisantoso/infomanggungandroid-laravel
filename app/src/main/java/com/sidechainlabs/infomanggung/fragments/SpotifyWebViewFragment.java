package com.sidechainlabs.infomanggung.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.controller.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Santoso on 12/13/2017.
 */

public class SpotifyWebViewFragment extends BaseFragment {
    public WebView mWebView;
    private String link,name;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    View view;

    public SpotifyWebViewFragment(){

    }

    public static SpotifyWebViewFragment newInstance(String link,String name) {
        Bundle args = new Bundle();
        args.putString("link", link);
        args.putString("name", name);
        SpotifyWebViewFragment fragment = new SpotifyWebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            link = getArguments().getString("link");
            name = getArguments().getString("name");

        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ( (MainActivity)getActivity()).updateToolbarTitle(name);
        view = inflater.inflate(R.layout.fragment_spotify_web_view, container, false);
        mWebView = view.findViewById(R.id.webview);
        mWebView.loadUrl(link);
        ButterKnife.bind(this,view);

        WebSettings webSettings = mWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);

        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(progress < 100 && mProgressBar.getVisibility() == ProgressBar.GONE){
                    mProgressBar.setVisibility(View.VISIBLE);
                }

                mProgressBar.setProgress(progress);
                if(progress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mWebView.destroy();
    }

    public void destroy(){
        mWebView.destroy();
        super.onDestroy();
    }

    @Override
    public void onPause(){
        mWebView.loadUrl("about:blank");
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        mWebView.loadUrl(link);
    }

}
