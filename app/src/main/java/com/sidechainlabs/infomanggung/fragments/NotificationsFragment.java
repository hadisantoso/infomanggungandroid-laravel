package com.sidechainlabs.infomanggung.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListNotificationsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.custommodel.NotificationResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mGigs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;


public class NotificationsFragment extends BaseFragment{

    View view;
    RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    ProgressBar progressBar;
    private List<mGigs> gigsList;
    private EndlessRecyclerViewScrollListener scrollListener;
    ListNotificationsAdapter listNotificationsAdapter;
    String gigsuri = "notifications";
    private Gson gson = new Gson();


    public static NotificationsFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        NotificationsFragment fragment = new NotificationsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).updateToolbarTitle("Notifications");
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_notifications, container, false);
        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        gigsList = new ArrayList<>();
        listNotificationsAdapter = new ListNotificationsAdapter(getActivity(),gigsList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listNotificationsAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        progressBar.setVisibility(View.VISIBLE);
        swipeContainer.setVisibility(View.GONE);
        addData(1);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mGigs gigs = gigsList.get(position);
                        DetailsGigsFragment detailsGigsFragment = new DetailsGigsFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsGigsFragment.newInstance(gigs.getId(),gigs.getName()));

                        }
                    }
                })
        );
        //ButterKnife.bind(this, view);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addData(1);
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void addData(final int page){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page",page);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(gigsuri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                if(page == 1){
                    gigsList.clear();
                    scrollListener.resetState();
                    listNotificationsAdapter.notifyDataSetChanged();
                }
                JSONObject jsonResult = null;
                JSONObject jsonResultGigs = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultGigs = jsonResult.getJSONObject("gigs");
                    data = jsonResultGigs.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GigsResponse notificationResponse = gson.fromJson(data,GigsResponse.class);
                for(int i=0;i<notificationResponse.getGigs().size();i++){
                    mGigs gig = notificationResponse.getGigs().get(i);
                    gig.setPhotoCover(awsPrefix+gig.getPhotoCover());
                    gig.setPhotoCoverThumbnail(awsPrefix+gig.getPhotoCoverThumbnail());
                    gigsList.add(gig);
                }
                listNotificationsAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.VISIBLE);
                swipeContainer.setRefreshing(false);
            }
        });

    }



}
