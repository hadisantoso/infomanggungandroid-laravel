package com.sidechainlabs.infomanggung.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sidechainlabs.infomanggung.R;
import com.sidechainlabs.infomanggung.adapter.ListArtistsAdapter;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.controller.MainActivity;
import com.sidechainlabs.infomanggung.custommodel.ArtistsResponse;
import com.sidechainlabs.infomanggung.listener.EndlessRecyclerViewScrollListener;
import com.sidechainlabs.infomanggung.listener.RecyclerItemClickListener;
import com.sidechainlabs.infomanggung.model.mArtist;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

/**
 * Created by Santoso on 11/24/2017.
 */

public class SearchArtistsFragment extends BaseFragment {

    int fragCount;
    private String queryParam;
    private List<mArtist> artistList;
    private RecyclerView recyclerView;
    ListArtistsAdapter listArtistsAdapter;
    ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;
    private EndlessRecyclerViewScrollListener scrollListener;
    private Gson gson = new Gson();
    String searchArtistsUri = "searchartists";
    View view;


    public static SearchArtistsFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(ARGS_INSTANCE, query);
        SearchArtistsFragment fragment = new SearchArtistsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public SearchArtistsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            queryParam = getArguments().getString(ARGS_INSTANCE);
        }
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ( (MainActivity)getActivity()).updateToolbarTitle("");
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_artists, container, false);
        recyclerView = view.findViewById(R.id.recycle_view);
        progressBar = view.findViewById(R.id.progress_bar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        artistList = new ArrayList<>();
        listArtistsAdapter = new ListArtistsAdapter(getActivity(),artistList);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listArtistsAdapter);
        addData(1,queryParam);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addData(++page,queryParam);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        mArtist artist = artistList.get(position);
                        DetailsArtistFragment detailsArtistFragment = new DetailsArtistFragment();
                        if (mFragmentNavigation != null) {
                            mFragmentNavigation.pushFragment(detailsArtistFragment.newInstance(artist.getId(),artist.getName()));

                        }
                    }
                })
        );

        swipeContainer.setEnabled(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void addData(int page, String queryParam){
        JSONObject reqObj = new JSONObject();
        try {
            reqObj.put("page",page);
            reqObj.put("query_param", queryParam);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GeneralMethods.requestData(searchArtistsUri, reqObj, getActivity(), new Callback() {
            @Override
            public void getData(String data) {
                JSONObject jsonResult = null;
                JSONObject jsonResultArtists = null;
                String awsPrefix = null;
                try {
                    jsonResult = new JSONObject(data);
                    jsonResultArtists = jsonResult.getJSONObject("artists");
                    data = jsonResultArtists.toString();
                    awsPrefix = jsonResult.getString("aws_prefix");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArtistsResponse artistsResponse = gson.fromJson(data,ArtistsResponse.class);
                for(int i = 0; i< artistsResponse.getArtists().size(); i++){
                    mArtist artist = artistsResponse.getArtists().get(i);
                    artist.setPhotoProfile(awsPrefix+artist.getPhotoProfile());
                    artist.setPhotoProfileThumbnail(awsPrefix+artist.getPhotoProfileThumbnail());
                    artistList.add(artist);
                }
                listArtistsAdapter.notifyDataSetChanged();
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Throwable error) {
                if ( error instanceof SocketTimeoutException || error instanceof ConnectException
                        || error instanceof ConnectTimeoutException) {
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(),getResources().getText(R.string.errormsg),Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) searchItem.getActionView();
        EditText txtSearch = ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        txtSearch.setTextColor(Color.WHITE);
        searchView.setQuery(queryParam,true);
        searchView.setIconified(false);
        searchView.clearFocus();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // Do something
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                artistList.clear();
                scrollListener.resetState();
                listArtistsAdapter.notifyDataSetChanged();
                queryParam = query;
                addData(1,queryParam);
                clearSearchViewFocus();
                return true;
            }
        };

        searchView.setOnQueryTextListener(queryTextListener);
    }


}
