package com.sidechainlabs.infomanggung.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by intel on 1/28/2017.
 */

public class GeneralMethods {
    private static Gson gson = new Gson();
    //    private static final String uri = "http://10.0.3.2:8000/v1/";
//    private static final String uri = "http://192.168.100.23:8000/v1/";
    private static final String uri = "https://api.infomanggung.com/v1/";
    private static FirebaseAuth mAuth;
    //    private static AsyncHttpClient client = new AsyncHttpClient();
    private static OkHttpClient okClient = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public static void saveData(Context context, String var, String data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(var, data).apply();
    }

    public static String getData(Context context, String var, String defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String result = prefs.getString(var, defValue);
        return result;
    }

    /**
     * Save Object Data to local shared preferences
     * @param var  Data name for object
     * @param data Object to save
     **/
    public static void saveObjectData(Context context, String var, Object data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String gData = gson.toJson(data);
        prefs.edit().putString(var, gData).apply();
    }

    /**
     * Get Object data from local shared preferences
     * @param var      Data name for object
     * @param defValue default value if data null
     * @param clazz    class object for return value
     **/
    public static Object getObjectData(Context context, String var, String defValue, Class clazz) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String data = prefs.getString(var, defValue);
        Object result = gson.fromJson(data, clazz);
        return result;
    }

    public static void clearData(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    public static String getServiceUri(String path) {
        return uri + path;
    }

    public static void getConnectionStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
           /* if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan

            }*/
        } else {
            Toast.makeText(context, "No internet connection...", Toast.LENGTH_SHORT).show();
        }
    }

    public static String getFirebaseIdToken() {
        mAuth = FirebaseAuth.getInstance();
        Task<GetTokenResult> task = mAuth.getCurrentUser().getToken(true)
                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {

                        } else {
                            // Handle error -> task.getException();
                        }
                    }
                });
        try {
            Tasks.await(task);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return task.getResult().getToken();
    }

//    public static void requestData(final String url, JSONObject request, final Context context, final Callback callback ){
//        mUser user = (mUser) GeneralMethods.getObjectData(context, "user", null, mUser.class);
//        client.setTimeout(GlobalValue.SOCKET_TIMEOUT);
//        try {
//            if(user!=null) {
//                request.put("user_id", user.getId().toString());
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        StringEntity entity = null;
//        try {
//            entity = new StringEntity(request.toString());
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        client.addHeader("Authorization","Bearer "+user.getApiToken());
//        client.post(context, GeneralMethods.getServiceUri(url), entity, "application/json", new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                String response = new String(responseBody);
//                callback.getData(response);
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                callback.onFailure(error);
//            }
//        });
//    }

    public static void requestData(final String url, JSONObject request, final Context context, final Callback callback) {
        mUser user = (mUser) GeneralMethods.getObjectData(context, "user", null, mUser.class);
        try {
            if (user != null) {
                request.put("user_id", user.getId().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, request.toString());

        Request req = new Request.Builder().url(GeneralMethods.getServiceUri(url)).post(body).addHeader("Authorization", "Bearer " + user.getApiToken()).build();
        okClient.newCall(req).enqueue(new MainThreadCallback() {
            @Override
            public void onFail(Exception error) {
                callback.onFailure(new Throwable(error));

            }

            @Override
            public void onSuccess(String responseBody) {
                callback.getData(responseBody);

            }
        });
    }

    public static void cancelRequest() {
        okClient.dispatcher().cancelAll();
    }

    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}