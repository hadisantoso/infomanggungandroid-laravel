package com.sidechainlabs.infomanggung.base;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.*;
import okhttp3.Callback;

/**
 * Created by Santoso on 12/4/2017.
 */

public abstract class MainThreadCallback implements Callback {
    private static final String TAG = MainThreadCallback.class.getSimpleName();

    abstract public void onFail(final Exception error);

    abstract public void onSuccess(final String responseBody);


    private void runOnUiThread(Runnable task) {
        new Handler(Looper.getMainLooper()).post(task);
    }

    @Override
    public void onFailure(Call call,final IOException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                e.printStackTrace();
                if (e.getMessage().contains("Canceled") || e.getMessage().contains("Socket closed")) {
                } else {
                    onFail(e);
                }
            }
        });
    }

    @Override
    public void onResponse(final Call call, final Response response) throws IOException {
        if (!response.isSuccessful() || response.body() == null) {
            onFailure(call, new IOException("Failed"));
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    onSuccess(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                    onFailure(call, new IOException("Failed"));
                }
            }
        });
    }
}
