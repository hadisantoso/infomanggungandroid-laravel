package com.sidechainlabs.infomanggung.base.CustomGlide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import static com.sidechainlabs.infomanggung.base.GlobalValue.BUCKET_NAME;
import static com.sidechainlabs.infomanggung.base.GlobalValue.PHOTO_TEMP_PATH;
import static com.sidechainlabs.infomanggung.base.GlobalValue.PHOTO_THUMBNAIL_TEMP_PATH;


public class DisplayImage {

    private static DisplayImage displayImage = new DisplayImage();

    private DisplayImage() {}

    public static DisplayImage getInstance () { return displayImage;}

    public void displayImageForUser (Context context, ImageView imageView, String uniqueId, RequestBuilder<Drawable> thumbnail) {

        ImageModel imageModel = new ImageModel();
        imageModel.setId(uniqueId);
        imageModel.setLocalPath(context.getFilesDir().getPath()+PHOTO_TEMP_PATH);
        imageModel.setBucketName(BUCKET_NAME);

        Glide.with(context)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter())
                .load(imageModel)
                .thumbnail(thumbnail)
                .into(imageView);
    }

    public RequestBuilder<Drawable> getImaage(Context context,String uniqueId){
        ImageModel imageModel = new ImageModel();
        imageModel.setId(uniqueId);
        imageModel.setLocalPath(context.getFilesDir().getPath()+PHOTO_THUMBNAIL_TEMP_PATH);
        imageModel.setBucketName(BUCKET_NAME);

        RequestBuilder<Drawable> result = Glide.with(context)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter())
                .load(imageModel);
        return result;
    }

}
