package com.sidechainlabs.infomanggung.base;

/**
 * Created by intel on 2/9/2017.
 */

public class GlobalValue {
    public static final int SOCKET_TIMEOUT = 15 * 1000;
    public static final String SUCCESS = "SUCCESS";
    public static final String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";
    public static final int AND_MORE_SIZE = 3;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    public static final String AWS_ACCESS_KEY = "";
    public static final String AWS_SECRET_KEY = "";
    public static final String BUCKET_NAME = "info-manggung";
    public static final String PHOTO_TEMP_PATH = "/image.jpg";
    public static final String PHOTO_THUMBNAIL_TEMP_PATH = "/image_thumbnail.jpg"; // store the downloaded image path

    public static final int ADD_EVENT_REQ_CODE = 20;



}
