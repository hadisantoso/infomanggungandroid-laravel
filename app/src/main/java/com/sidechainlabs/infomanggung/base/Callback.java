package com.sidechainlabs.infomanggung.base;

/**
 * Created by intel on 2/20/2017.
 */

public interface Callback {
    public void getData(String data);
    public void onFailure(Throwable e);
}
