package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mArtist;
import com.sidechainlabs.infomanggung.model.mGigs;

import java.util.List;

/**
 * Created by Santoso on 11/13/2017.
 */

public class ArtistsResponse {
    @SerializedName("data") private List<mArtist> artists;
    @SerializedName("aws_prerfix") private List<mGigs> awsPrefix;

    public List<mGigs> getAwsPrefix() {
        return awsPrefix;
    }

    public void setAwsPrefix(List<mGigs> awsPrefix) {
        this.awsPrefix = awsPrefix;
    }

    public List<mArtist> getArtists() {
        return artists;
    }

    public void setArtists(List<mArtist> artists) {
        this.artists = artists;
    }
}
