package com.sidechainlabs.infomanggung.custommodel;

import java.util.Map;

/**
 * Created by intel on 2/10/2017.
 */

public class ResponseMapper {
    private String message;
    private String status;
    private Map<String,Object> data;
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String,Object> getData() {
        return data;
    }
    public void setData(Map<String,Object> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
