package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mArtist;

import java.util.List;

/**
 * Created by Santoso on 10/29/2017.
 */

public class RecommendArtistsResponse {
    private List<mArtist> artists;
    @SerializedName("aws_prefix") private String awsPrefix;

    public String getAwsPrefix() {
        return awsPrefix;
    }

    public void setAwsPrefix(String awsPrefix) {
        this.awsPrefix = awsPrefix;
    }

    public List<mArtist> getArtists() {
        return artists;
    }

    public void setArtists(List<mArtist> artists) {
        this.artists = artists;
    }
}


