package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mUser;

import java.io.Serializable;

/**
 * Created by intel on 2/10/2017.
 */

public class LoginResponse extends ResponseMapper implements Serializable {
    private mUser user;
    @SerializedName("newuser") String isNewUser;

    public String getIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(String isNewUser) {
        this.isNewUser = isNewUser;
    }


    public mUser getUser() {
        return user;
    }

    public void setUser(mUser user) {
        this.user = user;
    }


}
