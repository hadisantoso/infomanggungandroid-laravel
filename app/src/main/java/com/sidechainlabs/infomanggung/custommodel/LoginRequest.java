package com.sidechainlabs.infomanggung.custommodel;

/**
 * Created by intel on 2/10/2017.
 */

public class LoginRequest {
    private String id;
    private String googleIdToken;
    private String facebookIdtoken;
    private String firebaseIdToken;
    private String provider;
    private String firebaseInstanceIdToken;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirebaseInstanceIdToken() {
        return firebaseInstanceIdToken;
    }

    public void setFirebaseInstanceIdToken(String firebaseInstanceIdToken) {
        this.firebaseInstanceIdToken = firebaseInstanceIdToken;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoogleIdToken() {
        return googleIdToken;
    }

    public void setGoogleIdToken(String googleIdToken) {
        this.googleIdToken = googleIdToken;
    }

    public String getFacebookIdtoken() {
        return facebookIdtoken;
    }

    public void setFacebookIdtoken(String facebookIdtoken) {
        this.facebookIdtoken = facebookIdtoken;
    }

    public String getFirebaseIdToken() {
        return firebaseIdToken;
    }

    public void setFirebaseIdToken(String firebaseIdToken) {
        this.firebaseIdToken = firebaseIdToken;
    }
}
