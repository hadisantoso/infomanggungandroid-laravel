package com.sidechainlabs.infomanggung.custommodel;

/**
 * Created by intel on 2/20/2017.
 */

public class RequestMapper {
    private String setFirebaseIdToken;

    public String getSetFirebaseIdToken() {
        return setFirebaseIdToken;
    }

    public void setSetFirebaseIdToken(String setFirebaseIdToken) {
        this.setFirebaseIdToken = setFirebaseIdToken;
    }
}
