package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mLocation;

import java.util.List;

/**
 * Created by Santoso on 12/3/2017.
 */

public class LocationResponse {
    @SerializedName("data") private List<mLocation> locations;

    public List<mLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<mLocation> locations) {
        this.locations = locations;
    }
}
