package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mGigs;

import java.util.List;

/**
 * Created by Santoso on 11/1/2017.
 */

public class GigsResponse {
    @SerializedName("data") private List<mGigs> gigs;
    @SerializedName("aws_prerfix") private List<mGigs> awsPrefix;

    public List<mGigs> getAwsPrefix() {
        return awsPrefix;
    }

    public void setAwsPrefix(List<mGigs> awsPrefix) {
        this.awsPrefix = awsPrefix;
    }

    public List<mGigs> getGigs() {
        return gigs;
    }

    public void setGigs(List<mGigs> gigs) {
        this.gigs = gigs;
    }
}
