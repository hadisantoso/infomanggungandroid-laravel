package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mArtist;

import java.util.List;

/**
 * Created by Santoso on 11/3/2017.
 */

public class GigsArtistsResponse {
    @SerializedName("artists") private List<mArtist> artists;

    public List<mArtist> getArtists() {
        return artists;
    }

    public void setArtists(List<mArtist> artists) {
        this.artists = artists;
    }
}
