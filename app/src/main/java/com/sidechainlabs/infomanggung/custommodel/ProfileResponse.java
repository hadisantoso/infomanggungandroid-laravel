package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Santoso on 12/5/2017.
 */

public class ProfileResponse {
    @SerializedName("count_gigs") private String countGigs;
    @SerializedName("count_locations") private String countLocations;
    @SerializedName("count_artists") private String countArtists;

    public String getCountGigs() {
        return countGigs;
    }

    public void setCountGigs(String countGigs) {
        this.countGigs = countGigs;
    }

    public String getCountLocations() {
        return countLocations;
    }

    public void setCountLocations(String countLocations) {
        this.countLocations = countLocations;
    }

    public String getCountArtists() {
        return countArtists;
    }

    public void setCountArtists(String countArtists) {
        this.countArtists = countArtists;
    }
}
