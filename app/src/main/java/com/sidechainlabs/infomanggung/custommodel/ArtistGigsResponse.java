package com.sidechainlabs.infomanggung.custommodel;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.model.mGigs;

import java.util.List;

/**
 * Created by Santoso on 11/23/2017.
 */

public class ArtistGigsResponse {
    @SerializedName("gigs") private List<mGigs> gigs;

    public List<mGigs> getGigs() {
        return gigs;
    }

    public void setGigs(List<mGigs> gigs) {
        this.gigs = gigs;
    }
}
