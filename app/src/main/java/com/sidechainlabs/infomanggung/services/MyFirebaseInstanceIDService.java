package com.sidechainlabs.infomanggung.services;

/**
 * Created by intel on 2/18/2017.
 */

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sidechainlabs.infomanggung.base.Callback;
import com.sidechainlabs.infomanggung.base.GeneralMethods;
import com.sidechainlabs.infomanggung.base.GlobalValue;
import com.sidechainlabs.infomanggung.base.MainThreadCallback;
import com.sidechainlabs.infomanggung.custommodel.GigsResponse;
import com.sidechainlabs.infomanggung.model.mGigs;
import com.sidechainlabs.infomanggung.model.mUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.entity.StringEntity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private static final String UPDATE_FIREBASE_TOKEN_URI = "updatefirbasetoken";
//    private static AsyncHttpClient client = new AsyncHttpClient();
    private static OkHttpClient okClient = new  OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Old Token:" + getOldToken());
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
//    private void sendRegistrationToServer(String token) {
//        JSONObject reqObj = new JSONObject();
//        try {
//            reqObj.put("firebaseInstanceId",token);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        mUser user = (mUser) GeneralMethods.getObjectData(this, "user", null, mUser.class);
//        client.setTimeout(GlobalValue.SOCKET_TIMEOUT);
//        try {
//            if(user!=null) {
//                reqObj.put("user_id", user.getId().toString());
//                client.addHeader("Authorization","Bearer "+user.getApiToken());
//                StringEntity entity = null;
//                try {
//                    entity = new StringEntity(reqObj.toString());
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                client.post(this, GeneralMethods.getServiceUri(UPDATE_FIREBASE_TOKEN_URI), entity, "application/json", new AsyncHttpResponseHandler() {
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                        String response = new String(responseBody);
//                        Log.i("Update Token","SUCCESS");
//                    }
//
//                    @Override
//                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                        Log.i("Update Token","FAILED");
//                    }
//                });
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void sendRegistrationToServer(final String token) {
        JSONObject request = new JSONObject();
        mUser user = (mUser) GeneralMethods.getObjectData(this, "user", null, mUser.class);
        try {
            request.put("firebaseInstanceIdToken",token);
            if(user!=null && getOldToken()!=null) {
                request.put("user_id", user.getId().toString());
                request.put("oldFirebaseInstanceIdToken",getOldToken());
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                RequestBody body = RequestBody.create(JSON,request.toString());

                Request req = new Request.Builder().url(GeneralMethods.getServiceUri(UPDATE_FIREBASE_TOKEN_URI)).post(body).addHeader("Authorization","Bearer "+user.getApiToken()).build();
                okClient.newCall(req).enqueue(new MainThreadCallback() {
                    @Override
                    public void onFail(Exception error) {
                        Log.i("Update Token","FAILED");
                    }

                    @Override
                    public void onSuccess(String responseBody) {
                        Log.i("Update Token","SUCCESS");
                        saveOldToken(token);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveOldToken(String token){
        GeneralMethods.saveData(this,"InstanceIdToken",token);
    }

    private String getOldToken(){
        String result = GeneralMethods.getData(this,"InstanceIdToken",null);
        return result;
    }
}