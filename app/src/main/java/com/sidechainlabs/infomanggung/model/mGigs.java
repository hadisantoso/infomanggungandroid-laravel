package com.sidechainlabs.infomanggung.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Santoso on 11/1/2017.
 */

public class mGigs {
    @SerializedName("id") private String id;
    @SerializedName("name") private String name;
    @SerializedName("description") private String description;
    @SerializedName("photo_cover") private String photoCover;
    @SerializedName("photo_cover_thumbnail") private String photoCoverThumbnail;
    @SerializedName("event_date") private String eventDate;
    @SerializedName("location") private mLocation location;
    @SerializedName("place") private String place;
    @SerializedName("latitude") private String latitude;
    @SerializedName("longitude") private String longitude;
    @SerializedName("artists") private String artists;
    @SerializedName("artist_name") private String artistName;


    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public mLocation getLocation() {
        return location;
    }

    public void setLocation(mLocation location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getPhotoCover() {
        return photoCover;
    }

    public void setPhotoCover(String photoCover) {
        this.photoCover = photoCover;
    }

    public String getPhotoCoverThumbnail() {
        return photoCoverThumbnail;
    }

    public void setPhotoCoverThumbnail(String photoCoverThumbnail) {
        this.photoCoverThumbnail = photoCoverThumbnail;
    }

    public mGigs() {
    }

    public mGigs(String name,String thumbnail) {
        this.name = name;
        this.photoCover = thumbnail;
    }
}
