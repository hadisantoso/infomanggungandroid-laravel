package com.sidechainlabs.infomanggung.model;

import com.google.gson.annotations.SerializedName;
import com.sidechainlabs.infomanggung.R;

public class mArtist {
    private String name;
    @SerializedName("photo_profile") private String photoProfile;
    @SerializedName("photo_profile_thumbnail") private String photoProfileThumbnail;
    @SerializedName("bio_indo") private String bioIndo;
    @SerializedName("bio_english") private String bioEnglish;
    @SerializedName("photo_cover") private String photoCover;
    @SerializedName("photo_cover_thumbnail") private String photoCoverThumbnail;
    @SerializedName("email") private String email;
    @SerializedName("phone") private String phone;
    @SerializedName("instagram") private String instagram;
    @SerializedName("twitter") private String twitter;
    @SerializedName("facebook") private String facebook;
    @SerializedName("soundcloud") private String soundcloud;
    @SerializedName("bandcamp") private String bandcamp;
    @SerializedName("reverbnation") private String reverbnation;
    @SerializedName("youtube") private String youtube;
    @SerializedName("spotify") private String spotify;
    @SerializedName("website") private String website;
    private String id;
    private boolean isFollowed = false; //still hardcode

    public String getSpotify() {
        return spotify;
    }

    public void setSpotify(String spotify) {
        this.spotify = spotify;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBioIndo() {
        return bioIndo;
    }

    public void setBioIndo(String bioIndo) {
        this.bioIndo = bioIndo;
    }

    public String getBioEnglish() {
        return bioEnglish;
    }

    public void setBioEnglish(String bioEnglish) {
        this.bioEnglish = bioEnglish;
    }

    public String getPhotoCover() {
        return photoCover;
    }

    public void setPhotoCover(String photoCover) {
        this.photoCover = photoCover;
    }

    public String getPhotoCoverThumbnail() {
        return photoCoverThumbnail;
    }

    public void setPhotoCoverThumbnail(String photoCoverThumbnail) {
        this.photoCoverThumbnail = photoCoverThumbnail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSoundcloud() {
        return soundcloud;
    }

    public void setSoundcloud(String soundcloud) {
        this.soundcloud = soundcloud;
    }

    public String getBandcamp() {
        return bandcamp;
    }

    public void setBandcamp(String bandcamp) {
        this.bandcamp = bandcamp;
    }

    public String getReverbnation() {
        return reverbnation;
    }

    public void setReverbnation(String reverbnation) {
        this.reverbnation = reverbnation;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public mArtist() {
    }

    public mArtist(String name, int numOfSongs, String photoProfile) {
        this.name = name;
        this.photoProfile = photoProfile;
    }

    public String getPhotoProfile() {
        return photoProfile;
    }

    public void setPhotoProfile(String photoProfile) {
        this.photoProfile = photoProfile;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoProfileThumbnail() {
        return photoProfileThumbnail;
    }

    public void setPhotoProfileThumbnail(String photoProfileThumbnail) {
        this.photoProfileThumbnail = photoProfileThumbnail;
    }
}