package com.sidechainlabs.infomanggung.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Santoso on 12/3/2017.
 */

public class mLocation {
    @SerializedName("id") private String id;
    @SerializedName("city_name") private String cityName;
    @SerializedName("google_place_id") private String googlePlaceId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }
}

