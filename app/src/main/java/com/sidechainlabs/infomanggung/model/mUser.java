package com.sidechainlabs.infomanggung.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by intel on 1/27/2017.
 */

public class mUser implements Serializable{

    @SerializedName("name")private String fullName;
    private String email;
    private String id;
    private String firebaseIdToken;
    private String googleIdToken;
    private String facebookIdToken;
    @SerializedName("photo_profile") private String pictureUri;
    private String provider;

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    @SerializedName("api_token") private String apiToken;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFacebookIdToken() {
        return facebookIdToken;
    }

    public void setFacebookIdToken(String facebookIdToken) {
        this.facebookIdToken = facebookIdToken;
    }

    public String getGoogleIdToken() {
        return googleIdToken;
    }

    public void setGoogleIdToken(String googleIdToken) {
        this.googleIdToken = googleIdToken;
    }

    public String getFirebaseIdToken() {
        return firebaseIdToken;
    }

    public void setFirebaseIdToken(String firebaseIdToken) {
        this.firebaseIdToken = firebaseIdToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
